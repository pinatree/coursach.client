﻿using Coursach.DbConnection.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursach.DbConnection.EducationLevelsRepositories
{
	public interface IEducationLevelsRepository : Listable<EducationLevel>
	{
		//GET
		EducationLevel GetEducationLevel(int id);

		//POST
		int UpdateEducationLevel(EducationLevel eduLevel);

		//PUT
		int AddEducationLevel(EducationLevel eduLevel);

		//DELETE
		int DeleteEducationLevel(int id);
	}
}
