﻿using Coursach.DbConnection.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;

namespace Coursach.DbConnection.EducationLevelsRepositories
{
	public class MDBEducationLevelsRepository : IEducationLevelsRepository
	{
		DbConnection DbConnection;

		public MDBEducationLevelsRepository(DbConnection dbConnection)
		{
			this.DbConnection = dbConnection;
		}

		public int Count
		{
			get
			{
				OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
				cmd.CommandText = "SELECT COUNT (*) as Total FROM УровеньОбразования";
				OleDbDataReader reader = cmd.ExecuteReader();

				DataTable myDataTable = new DataTable();
				myDataTable.Load(reader);

				return Convert.ToInt32(myDataTable.Rows[0][0]);
			}
		}

		public int AddEducationLevel(EducationLevel eduLevel)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "INSERT INTO УровеньОбразования (Название) Values('" + eduLevel.Name + "')";
			return cmd.ExecuteNonQuery();
		}

		public int DeleteEducationLevel(int id)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "DELETE * FROM УровеньОбразования WHERE УровеньОбразования.Код=" + id.ToString();
			return cmd.ExecuteNonQuery();
		}

		public EducationLevel GetEducationLevel(int id)
		{
			throw new NotImplementedException();
		}

		public IEnumerable<EducationLevel> List()
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "SELECT * FROM УровеньОбразования";
			OleDbDataReader reader = cmd.ExecuteReader();

			DataTable myDataTable = new DataTable();
			myDataTable.Load(reader);

			List<EducationLevel> eduLevels = new List<EducationLevel>();

			foreach (DataRow eduLevelRow in myDataTable.Rows)
			{
				eduLevels.Add(RowToEduLevel(eduLevelRow));
			}

			return eduLevels;
		}

		public int UpdateEducationLevel(EducationLevel eduLevel)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "UPDATE УровеньОбразования "
				+ "SET "
				+ "УровеньОбразования.Название = '" + eduLevel.Name + "' "
				+ "WHERE УровеньОбразования.Код = " + eduLevel.Id.ToString();
			var changed =  cmd.ExecuteNonQuery();

			return changed;
		}

		EducationLevel RowToEduLevel(DataRow row)
		{
			EducationLevel eduLevel = new EducationLevel(
				this,
				id: Convert.ToInt32(row["Код"]),
				name: row["Название"].ToString());

			return eduLevel;
		}
	}
}
