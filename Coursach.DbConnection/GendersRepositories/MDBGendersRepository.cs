﻿using Coursach.DbConnection.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;

namespace Coursach.DbConnection.GendersRepositories
{
	public class MDBGendersRepository : IGendersRepository
	{
		DbConnection DbConnection;

		public MDBGendersRepository(DbConnection dbConnection)
		{
			this.DbConnection = dbConnection;
		}

		public int Count
		{
			get
			{
				OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
				cmd.CommandText = "SELECT COUNT (*) as Total FROM Пол";
				OleDbDataReader reader = cmd.ExecuteReader();

				DataTable myDataTable = new DataTable();
				myDataTable.Load(reader);

				return Convert.ToInt32(myDataTable.Rows[0][0]);
			}
		}

		public int AddGender(Gender gender)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "INSERT INTO Пол (Пол) Values('" + gender.Name + "')";
			return cmd.ExecuteNonQuery();
		}

		public int DeleteGender(int id)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "DELETE * FROM Пол WHERE Пол.Код=" + id.ToString();
			return cmd.ExecuteNonQuery();
		}

		public Gender GetGender(int id)
		{
			throw new NotImplementedException();
		}

		public IEnumerable<Gender> List()
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "SELECT * FROM Пол";
			OleDbDataReader reader = cmd.ExecuteReader();

			DataTable myDataTable = new DataTable();
			myDataTable.Load(reader);

			List<Gender> genders = new List<Gender>();

			foreach (DataRow vacancyRow in myDataTable.Rows)
			{
				genders.Add(RowToGender(vacancyRow));
			}

			return genders;
		}

		public int UpdateGender(Gender gender)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "UPDATE Пол "
				+ "SET "
				+ "Пол.Пол = '" + gender.Name + "' "
				+ "WHERE Пол.Код = " + gender.Id.ToString();
			return cmd.ExecuteNonQuery();
		}

		Gender RowToGender(DataRow row)
		{
			Gender newGender = new Gender(
				repository: this,
				id: Convert.ToInt32(row["Код"]),
				name: row["Пол"].ToString());

			return newGender;
		}
	}
}
