﻿using Coursach.DbConnection.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursach.DbConnection.GendersRepositories
{
	public interface IGendersRepository : Listable<Gender>
	{
		//GET
		Gender GetGender(int id);

		//POST
		int UpdateGender(Gender gender);

		//PUT
		int AddGender(Gender gender);

		//DELETE
		int DeleteGender(int id);
	}
}
