﻿using Coursach.DbConnection.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursach.DbConnection.Repositories
{
	public interface ICitizensRepository : Listable<Citizen>
	{
		//GET
		Citizen GetCitizen(string passport);

		//POST
		int UpdateCitizen(Citizen citizen);

		//PUT
		int AddCitizen(Citizen citizen);

		//DELETE
		int DeleteCitizen(string passport);

		int UpdatePassport(string oldPassport, string newPassport);
	}
}
