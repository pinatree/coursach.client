﻿using Coursach.DbConnection.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;

namespace Coursach.DbConnection.Repositories
{
	public class MDBCitizensRepository : ICitizensRepository
	{
		//Соединение с БД
		DbConnection DbConnection { get; }

		public MDBCitizensRepository(DbConnection dbConnection)
		{
			this.DbConnection = dbConnection;
		}

		//Количество граждан
		public int Count
		{
			get
			{
				OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
				cmd.CommandText = "SELECT COUNT (*) as Total FROM Гражданин";
				OleDbDataReader reader = cmd.ExecuteReader();

				DataTable myDataTable = new DataTable();
				myDataTable.Load(reader);

				return Convert.ToInt32(myDataTable.Rows[0][0]);
			}
		}

		//Получаем конкретного гражданина
		public Citizen GetCitizen(string passport)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "SELECT TOP 1 * FROM Гражданин INNER JOIN Пол ON Гражданин.Пол = Пол.Код where (Паспорт = '" + passport + "')";
			OleDbDataReader reader = cmd.ExecuteReader();

			DataTable myDataTable = new DataTable();
			myDataTable.Load(reader);

			var first = myDataTable.Rows[0];

			Citizen citizen = RowToCitizen(first);

			return citizen;
		}

		//Список всех граждан
		public IEnumerable<Citizen> List()
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "SELECT * FROM Гражданин INNER JOIN Пол ON Гражданин.Пол = Пол.Код";
			OleDbDataReader reader = cmd.ExecuteReader();

			DataTable myDataTable = new DataTable();
			myDataTable.Load(reader);

			List<Citizen> citizens = new List<Citizen>();

			foreach(DataRow citizenRow in myDataTable.Rows)
			{
				citizens.Add(RowToCitizen(citizenRow));
			}

			return citizens;
		}

		//Обновление гражданина
		public int UpdateCitizen(Citizen citizen)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "UPDATE Гражданин "
				+ "SET "
				+ "Гражданин.Имя = '" + citizen.Name + "',"
				+ "Гражданин.Фамилия = '" + citizen.Surname + "',"
				+ "Гражданин.Отчество = '" + citizen.Patronymic + "',"
				+ "Гражданин.Пол = '" + citizen.GenderId.ToString() + "',"
				+ "Гражданин.ДРожд = #" + citizen.BirthDate.ToString("MM/dd/yyyy HH:mm:ss").Replace('.', '/') + "#"
				+ "WHERE Гражданин.Паспорт = '" + citizen.Passport.ToString() + "'";
			return cmd.ExecuteNonQuery();
		}

		//Добавление гражданина
		public int AddCitizen(Citizen citizen)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "INSERT INTO Гражданин VALUES ('"
				+ citizen.Passport + "','"
				+ citizen.Surname + "','"
				+ citizen.Name + "','"
				+ citizen.Patronymic + "',"
				+ citizen.GenderId.ToString() + ",'"
				+ citizen.BirthDate.ToString("MM/dd/yyyy HH:mm:ss").Replace('.', '/') + "')";
			return cmd.ExecuteNonQuery();
		}

		//Удаление гражданина
		public int DeleteCitizen(string passport)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "DELETE FROM Гражданин WHERE Паспорт = '" + passport + "'";
			return cmd.ExecuteNonQuery();
		}

		//Обновление паспорта гражданина
		public int UpdatePassport(string oldPassport, string newPassport)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "UPDATE Гражданин SET Гражданин.Паспорт = '" + newPassport + "' " +
				"WHERE (Гражданин.Паспорт = '" + oldPassport + "');";
			return cmd.ExecuteNonQuery();
		}

		//Приватный метод для получения экземпляра класса Citizen из взятой из БД строки
		Citizen RowToCitizen(DataRow row)
		{
			Citizen newCitizen = new Citizen(
				repository: this,
				passport: row["Паспорт"].ToString(),
				surname: row["Фамилия"].ToString(),
				name: row["Имя"].ToString(),
				patronimyc: row["Отчество"].ToString(),
				genderId: (int)row["Код"],
				birthDate: Convert.ToDateTime(row["ДРожд"])
				);

			Gender citizenGender = new Gender(
				id: Convert.ToInt32(row["Код"]),
				name: row["Пол.Пол"].ToString());

			newCitizen.Gender = citizenGender;

			return newCitizen;
		}
	}
}
