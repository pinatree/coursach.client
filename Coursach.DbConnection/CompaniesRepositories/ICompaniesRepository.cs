﻿using Coursach.DbConnection.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursach.DbConnection.Repositories
{
	public interface ICompaniesRepository : Listable<Company>
	{
		//GET
		Company GetCompany(string registration_number);

		//POST
		int UpdateCompany(Company company);

		//PUT
		int AddCompany(Company company);

		//DELETE
		int DeleteCompany(string registration_number);

		int UpdateRegistrationNumber(string oldNum, string newNum);
	}
}
