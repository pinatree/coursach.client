﻿using Coursach.DbConnection.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;

namespace Coursach.DbConnection.Repositories
{
	public class MDBCompaniesRepository : ICompaniesRepository
	{
		DbConnection DbConnection;

		public MDBCompaniesRepository(DbConnection dbConnection)
		{
			this.DbConnection = dbConnection;
		}

		public int Count
		{
			get
			{
				OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
				cmd.CommandText = "SELECT COUNT (*) as Total FROM Компания";
				OleDbDataReader reader = cmd.ExecuteReader();

				DataTable myDataTable = new DataTable();
				myDataTable.Load(reader);

				return Convert.ToInt32(myDataTable.Rows[0][0]);
			}
		}


		public Company GetCompany(string registration_number)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "SELECT TOP 1 * FROM Компания where (РегНомКомпании = '" + registration_number + "')";
			OleDbDataReader reader = cmd.ExecuteReader();

			DataTable myDataTable = new DataTable();
			myDataTable.Load(reader);

			var first = myDataTable.Rows[0];

			Company company = RowToCompany(first);

			return company;
		}

		public IEnumerable<Company> List()
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "SELECT * FROM Компания";
			OleDbDataReader reader = cmd.ExecuteReader();

			DataTable myDataTable = new DataTable();
			myDataTable.Load(reader);

			List<Company> companies = new List<Company>();

			foreach (DataRow companyRow in myDataTable.Rows)
			{
				companies.Add(RowToCompany(companyRow));
			}

			return companies;
		}

		Company RowToCompany(DataRow row)
		{
			Company newCompany = new Company(
				this,
				regNumber: row["РегНомКомпании"].ToString(),
				name: row["Название"].ToString(),
				address: row["Адрес"].ToString());

			return newCompany;
		}

		public int AddCompany(Company company)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "INSERT INTO Компания VALUES ('"
				+ company.CompanyRegistrationNumber + "','"
				+ company.Name + "','"
				+ company.Address + "')";
			return cmd.ExecuteNonQuery();
		}

		public int DeleteCompany(string registration_number)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "DELETE FROM Компания WHERE РегНомКомпании = '" + registration_number + "'";
			return cmd.ExecuteNonQuery();
		}

		public int UpdateCompany(Company company)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "UPDATE Компания "
				+ "SET "
				+ "Компания.Название = '" + company.Name + "', "
				+ "Компания.Адрес = '" + company.Address + "' "
				+ "WHERE Компания.РегНомКомпании = '" + company.CompanyRegistrationNumber + "'";
			return cmd.ExecuteNonQuery();
		}

		public int UpdateRegistrationNumber(string oldNum, string newNum)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "UPDATE Компания SET Компания.РегНомКомпании = '" + newNum + "' " +
				"WHERE (Компания.РегНомКомпании = '" + oldNum + "');";
			return cmd.ExecuteNonQuery();
		}
	}
}
