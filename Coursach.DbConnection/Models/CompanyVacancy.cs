﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Coursach.DbConnection.Models
{
	public class CompanyVacancy : INotifyPropertyChanged
	{		
		public CompanyVacancy(string regNumber, int vacancyId, DateTime creationDate, bool opened)
		{
			this.CompanyRegNumber = regNumber;
			this.VacancyId = vacancyId;
			this.CreationDate = creationDate;
			this.Opened = opened;
		}

		private string companyRegNumber;
		private int vacancyId;
		private DateTime creationDate;
		private bool opened;

		public string CompanyRegNumber
		{
			get => companyRegNumber;
			set
			{
				if(value != companyRegNumber)
				{
					companyRegNumber = value;
					NotifyPropertyChanged();
				}
			}
		}

		public int VacancyId
		{
			get => vacancyId;
			set
			{
				if (value != vacancyId)
				{
					vacancyId = value;
					NotifyPropertyChanged();
				}
			}
		}

		public DateTime CreationDate
		{
			get => creationDate;
			set
			{
				if (value != creationDate)
				{
					creationDate = value;
					NotifyPropertyChanged();
				}
			}
		}

		public bool Opened
		{
			get => opened;
			set
			{
				if (value != opened)
				{
					opened = value;
					NotifyPropertyChanged();
				}
			}
		}

		public Vacancy Vacancy { get; set; }

		public Company Company { get; set; }



		#region NotifyPropertyChanged

		public event PropertyChangedEventHandler PropertyChanged;

		private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion NotifyPropertyChanged
	}
}
