﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Coursach.DbConnection.Models
{
	public class Interview : INotifyPropertyChanged
	{
		private string passport;
		private string companyRegistrationNumber;
		private int vacancyId;
		private DateTime creationDate;
		private DateTime interviewDate;
		private bool success;

		public Interview(string passport, string comRegNum, int vacancyId, DateTime creationDate, DateTime interviewDate, bool success)
		{
			this.Passport = passport;
			this.CompanyRegistrationNumber = comRegNum;
			this.VacancyId = vacancyId;
			this.CreationDate = creationDate;
			this.InterviewDate = interviewDate;
			this.Success = success;
		}


		public string Passport
		{
			get => passport;
			set
			{
				if(value != passport)
				{
					passport = value;
					NotifyPropertyChanged();
				}
			}
		}

		public string CompanyRegistrationNumber
		{
			get => companyRegistrationNumber;
			set
			{
				if (value != companyRegistrationNumber)
				{
					companyRegistrationNumber = value;
					NotifyPropertyChanged();
				}
			}
		}

		public int VacancyId
		{
			get => vacancyId;
			set
			{
				if (value != vacancyId)
				{
					vacancyId = value;
					NotifyPropertyChanged();
				}
			}
		}

		public DateTime CreationDate
		{
			get => creationDate;
			set
			{
				if (value != creationDate)
				{
					creationDate = value;
					NotifyPropertyChanged();
				}
			}
		}

		public DateTime InterviewDate
		{
			get => interviewDate;
			set
			{
				if (value != interviewDate)
				{
					interviewDate = value;
					NotifyPropertyChanged();
				}
			}
		}

		public bool Success
		{
			get => success;
			set
			{
				if (value != success)
				{
					success = value;
					NotifyPropertyChanged();
				}
			}
		}

		public Citizen Citizen { get; set; }

		public CompanyVacancy CompanyVacancy { get; set; }



		#region NotifyPropertyChanged

		public event PropertyChangedEventHandler PropertyChanged;

		private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion NotifyPropertyChanged
	}
}
