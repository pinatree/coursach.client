﻿using Coursach.DbConnection.Repositories;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Coursach.DbConnection.Models
{
	public class Vacancy : INotifyPropertyChanged
	{
		IVacanciesRepository Repository;

		public Vacancy(int id, string name)
		{
			this.Id = id;
			this.Name = name;
		}

		public Vacancy(IVacanciesRepository repository, int id, string name)
		{
			this.Id = id;
			this.Name = name;

			this.Repository = repository;
		}

		private int id;
		public int Id
		{
			get => id;
			set
			{
				if (value != id)
				{
					id = value;
					Repository?.UpdateVacancy(this);
					NotifyPropertyChanged();
				}
			}
		}

		private string name;
		public string Name
		{
			get => name;
			set
			{
				if (value != name)
				{
					name = value;
					Repository?.UpdateVacancy(this);
					NotifyPropertyChanged();
				}
			}
		}

		#region NotifyPropertyChanged

		public event PropertyChangedEventHandler PropertyChanged;

		private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion NotifyPropertyChanged
	}
}
