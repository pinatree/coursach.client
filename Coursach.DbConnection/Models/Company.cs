﻿using Coursach.DbConnection.Repositories;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Coursach.DbConnection.Models
{
	public class Company : INotifyPropertyChanged
	{
		ICompaniesRepository Repository;

		public Company(string regNumber, string name, string address)
		{
			this.CompanyRegistrationNumber = regNumber;
			this.Name = name;
			this.Address = address;
		}

		public Company(ICompaniesRepository repository, string regNumber, string name, string address)
		{
			this.CompanyRegistrationNumber = regNumber;
			this.Name = name;
			this.Address = address;

			this.Repository = repository;
		}

		private string companyRegistrationNumber;
		public string CompanyRegistrationNumber
		{
			get => companyRegistrationNumber;
			set
			{
				if (value != companyRegistrationNumber)
				{
					companyRegistrationNumber = value;
					Repository?.UpdateCompany(this);
					NotifyPropertyChanged();
				}
			}
		}

		private string name;
		public string Name
		{
			get => name;
			set
			{
				if (value != name)
				{
					name = value;
					Repository?.UpdateCompany(this);
					NotifyPropertyChanged();
				}
			}
		}

		private string address;
		public string Address
		{
			get => address;
			set
			{
				if (value != address)
				{
					address = value;
					Repository?.UpdateCompany(this);
					NotifyPropertyChanged();
				}
			}
		}

		#region NotifyPropertyChanged

		public event PropertyChangedEventHandler PropertyChanged;

		private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion NotifyPropertyChanged
	}
}
