﻿using Coursach.DbConnection.EducationLevelsRepositories;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Coursach.DbConnection.Models
{
	public class EducationLevel : INotifyPropertyChanged
	{
		IEducationLevelsRepository Repository;

		public EducationLevel(int id, string name)
		{
			this.Id = id;
			this.Name = name;
		}

		public EducationLevel(IEducationLevelsRepository repository, int id, string name)
		{
			this.Id = id;
			this.Name = name;

			this.Repository = repository;
		}

		private int id;
		public int Id
		{
			get => id;
			set
			{
				if (value != id)
				{
					id = value;
					Repository?.UpdateEducationLevel(this);
					NotifyPropertyChanged();
				}
			}
		}

		private string name;
		public string Name
		{
			get => name;
			set
			{
				if (value != name)
				{
					name = value;
					Repository?.UpdateEducationLevel(this);
					NotifyPropertyChanged();
				}
			}
		}

		#region NotifyPropertyChanged

		public event PropertyChangedEventHandler PropertyChanged;

		private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion NotifyPropertyChanged
	}
}
