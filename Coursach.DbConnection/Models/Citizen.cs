﻿using Coursach.DbConnection.Repositories;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Coursach.DbConnection.Models
{
	public class Citizen : INotifyPropertyChanged
	{
		public ICitizensRepository Repository;

		public Citizen(string passport, string surname, string name,
			string patronimyc, int genderId, DateTime birthDate)
		{
			this.Passport = passport;
			this.Surname = surname;
			this.Name = name;
			this.Patronymic = patronimyc;
			this.GenderId = genderId;
			this.BirthDate = birthDate;
		}

		public Citizen(ICitizensRepository repository, string passport, string surname, string name,
			string patronimyc, int genderId, DateTime birthDate)
		{
			this.Passport = passport;
			this.Surname = surname;
			this.Name = name;
			this.Patronymic = patronimyc;
			this.GenderId = genderId;
			this.BirthDate = birthDate;

			this.Repository = repository;
		}

		private string passport;
		public string Passport
		{
			get => passport;
			set
			{
				if (value != passport)
				{
					passport = value;
					Repository?.UpdateCitizen(this);
					NotifyPropertyChanged();
				}
			}
		}

		private string surname { get; set; }
		public string Surname
		{
			get => surname;
			set
			{
				if (value != surname)
				{
					surname = value;
					Repository?.UpdateCitizen(this);
					NotifyPropertyChanged();
				}
			}
		}

		private string name;
		public string Name
		{
			get => name;
			set
			{
				if (value != name)
				{
					name = value;
					Repository?.UpdateCitizen(this);
					NotifyPropertyChanged();
				}
			}
		}

		private string patronymic;
		public string Patronymic
		{
			get => patronymic;
			set
			{
				if (value != patronymic)
				{
					patronymic = value;
					Repository?.UpdateCitizen(this);
					NotifyPropertyChanged();
				}
			}
		}

		private int genderId;
		public int GenderId
		{
			get => genderId;
			set
			{
				if (value != genderId)
				{
					genderId = value;
					Repository?.UpdateCitizen(this);
					NotifyPropertyChanged();
				}
			}
		}

		private DateTime birthDate;
		public DateTime BirthDate
		{
			get => birthDate;
			set
			{
				if (value != birthDate)
				{
					birthDate = value;
					Repository?.UpdateCitizen(this);
					NotifyPropertyChanged();
				}
			}
		}

		//included
		public Gender Gender { get; set; }

		#region NotifyPropertyChanged

		public event PropertyChangedEventHandler PropertyChanged;

		private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion NotifyPropertyChanged
	}
}
