﻿using Coursach.DbConnection.DiplomasRepositories;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Coursach.DbConnection.Models
{
	public class Diploma : INotifyPropertyChanged
	{
		IDiplomasRepository Repository;
		private string registrationNumber;
		private DateTime issueDate;
		private string specialty;
		private int educationLevelId;
		private int instituteId;
		private string passport;

		public Diploma(string regNumber, string passport, int instititeId, int eduLevelId,
			string speciality, DateTime issueDate)
		{
			this.RegistrationNumber = regNumber;
			this.Passport = passport;
			this.InstituteId = instititeId;
			this.EducationLevelId = eduLevelId;
			this.Specialty = speciality;
			this.IssueDate = issueDate;
		}

		public Diploma(IDiplomasRepository repository, string regNumber, string passport, int instititeId,
			int eduLevelId, string speciality, DateTime issueDate)
		{
			this.RegistrationNumber = regNumber;
			this.Passport = passport;
			this.InstituteId = instititeId;
			this.EducationLevelId = eduLevelId;
			this.Specialty = speciality;
			this.IssueDate = issueDate;

			this.Repository = repository;
		}

		public string RegistrationNumber
		{
			get => registrationNumber;
			set
			{
				if (value != registrationNumber)
				{
					registrationNumber = value;
					Repository?.UpdateDiploma(this);
					NotifyPropertyChanged();
				}
			}
		}

		public string Passport
		{
			get => passport;
			set
			{
				if (value != passport)
				{
					passport = value;
					Repository?.UpdateDiploma(this);
					NotifyPropertyChanged();
				}
			}
		}

		public int InstituteId
		{
			get => instituteId;
			set
			{
				if (value != instituteId)
				{
					instituteId = value;
					Repository?.UpdateDiploma(this);
					NotifyPropertyChanged();
				}
			}
		}

		public int EducationLevelId
		{
			get => educationLevelId;
			set
			{
				if (value != educationLevelId)
				{
					educationLevelId = value;
					Repository?.UpdateDiploma(this);
					NotifyPropertyChanged();
				}
			}
		}

		public string Specialty
		{
			get => specialty;
			set
			{
				if (value != specialty)
				{
					specialty = value;
					Repository?.UpdateDiploma(this);
					NotifyPropertyChanged();
				}
			}
		}

		public DateTime IssueDate
		{
			get => issueDate;
			set
			{
				if (value != issueDate)
				{
					issueDate = value;
					Repository?.UpdateDiploma(this);
					NotifyPropertyChanged();
				}
			}
		}

		public EducationLevel EducationLevel { get; set; }
		public Institute Institute { get; set; }


		#region NotifyPropertyChanged

		public event PropertyChangedEventHandler PropertyChanged;

		private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion NotifyPropertyChanged
	}
}
