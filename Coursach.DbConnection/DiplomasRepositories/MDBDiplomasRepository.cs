﻿using Coursach.DbConnection.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;

namespace Coursach.DbConnection.DiplomasRepositories
{
	public class MDBDiplomasRepository : IDiplomasRepository
	{
		DbConnection DbConnection;

		public MDBDiplomasRepository(DbConnection dbConnection)
		{
			this.DbConnection = dbConnection;
		}


		public int Count
		{
			get
			{
				OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
				cmd.CommandText = "SELECT COUNT (*) as Total FROM Диплом";
				OleDbDataReader reader = cmd.ExecuteReader();

				DataTable myDataTable = new DataTable();
				myDataTable.Load(reader);

				return Convert.ToInt32(myDataTable.Rows[0][0]);
			}
		}

		public Diploma GetDiploma(string registrationNumber)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "SELECT TOP 1 * FROM Диплом WHERE РегНомДипл = '" + registrationNumber + "'";
			OleDbDataReader reader = cmd.ExecuteReader();

			DataTable myDataTable = new DataTable();
			myDataTable.Load(reader);

			DataRow first = myDataTable.Rows[0];

			Diploma converted = RowToDiploma(first);

			return converted;
		}

		public IEnumerable<Diploma> List()
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "SELECT Диплом.РегНомДипл, Диплом.Паспорт, " +
				"Диплом.УчЗав AS КодУчЗав, Диплом.УровОбраз AS КодУрОбраз, " +
				"Диплом.НаправлПодготов, Диплом.ДатаВыдачи, " +
				"УровеньОбразования.Название AS НазвУрОбраз, " +
				"УчебноеЗаведение.Название AS УчЗавНазв " +
				"FROM УчебноеЗаведение INNER JOIN (УровеньОбразования INNER JOIN Диплом ON " +
				"УровеньОбразования.Код = Диплом.УровОбраз) ON УчебноеЗаведение.Код = Диплом.УчЗав;";
			OleDbDataReader reader = cmd.ExecuteReader();

			DataTable myDataTable = new DataTable();
			myDataTable.Load(reader);

			List<Diploma> diplomas = new List<Diploma>();

			foreach (DataRow diplomaRow in myDataTable.Rows)
			{
				diplomas.Add(RowToDiploma(diplomaRow));
			}

			return diplomas;
		}

		public int AddDiploma(Diploma diploma)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "INSERT INTO Диплом VALUES ('"
				+ diploma.RegistrationNumber + "','"
				+ diploma.Passport + "',"
				+ diploma.InstituteId + ","
				+ diploma.EducationLevelId + ",'"
				+ diploma.Specialty + "','"
				+ diploma.IssueDate.ToString().Replace('.', '/') + "')";

			return cmd.ExecuteNonQuery();
		}

		public int DeleteDiploma(string registrationNumber)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "DELETE FROM Диплом WHERE РегНомДипл = '" + registrationNumber + "'";
			return cmd.ExecuteNonQuery();
		}

		public int UpdateDiploma(Diploma diploma)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "UPDATE Диплом "
				+ "SET "
				+ "Диплом.Паспорт = '" + diploma.Passport + "',"
				+ "Диплом.УчЗав = '" + diploma.InstituteId + "',"
				+ "Диплом.УровОбраз = '" + diploma.EducationLevelId + "',"
				+ "Диплом.НаправлПодготов = '" + diploma.Specialty + "',"
				+ "Диплом.ДатаВыдачи = '" + diploma.IssueDate.ToString().Replace('.', '/') + "'"
				+ "WHERE Диплом.РегНомДипл = '" + diploma.RegistrationNumber.ToString() + "'";
			var changed = cmd.ExecuteNonQuery();

			if (changed == 0)
				throw new Exception();

			return changed;
		}

		public int UpdateRegNumber(string oldRegNumber, string newRegNumber)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "UPDATE Диплом SET Диплом.РегНомДипл = '" + newRegNumber + "' " +
				"WHERE (Диплом.РегНомДипл = '" + oldRegNumber + "');";
			return cmd.ExecuteNonQuery();
		}

		Diploma RowToDiploma(DataRow row)
		{
			Diploma newDiploma = new Diploma(
				this,
				regNumber: row["РегНомДипл"].ToString(),
				passport: row["Паспорт"].ToString(),
				instititeId: Convert.ToInt32(row["КодУчЗав"]),
				eduLevelId: Convert.ToInt32(row["КодУрОбраз"]),
				speciality: row["НаправлПодготов"].ToString(),
				issueDate: Convert.ToDateTime(row["ДатаВыдачи"]));

			Institute institute = new Institute(
				id: Convert.ToInt32(row["КодУчЗав"]),
				name: row["УчЗавНазв"].ToString());

			EducationLevel educationLevel = new EducationLevel(
				id: Convert.ToInt32(row["КодУрОбраз"]),
				name: row["НазвУрОбраз"].ToString());

			newDiploma.EducationLevel = educationLevel;
			newDiploma.Institute = institute;

			return newDiploma;
		}
	}
}
