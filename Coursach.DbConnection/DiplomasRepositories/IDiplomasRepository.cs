﻿using Coursach.DbConnection.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursach.DbConnection.DiplomasRepositories
{
	public interface IDiplomasRepository : Listable<Diploma>
	{
		Diploma GetDiploma(string registrationNumber);

		int AddDiploma(Diploma diploma);

		int DeleteDiploma(string registrationNumber);

		int UpdateDiploma(Diploma diploma);

		int UpdateRegNumber(string oldRegNumber, string newRegNumber);
	}
}
