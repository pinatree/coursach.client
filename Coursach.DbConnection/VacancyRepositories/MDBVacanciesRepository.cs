﻿using Coursach.DbConnection.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;

namespace Coursach.DbConnection.Repositories
{
	public class MDBVacanciesRepository : IVacanciesRepository
	{
		DbConnection DbConnection;

		public int Count
		{
			get
			{
				OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
				cmd.CommandText = "SELECT COUNT (*) as Total FROM Вакансия";
				OleDbDataReader reader = cmd.ExecuteReader();

				DataTable myDataTable = new DataTable();
				myDataTable.Load(reader);

				return Convert.ToInt32(myDataTable.Rows[0][0]);
			}
		}

		public MDBVacanciesRepository(DbConnection dbConnection)
		{
			this.DbConnection = dbConnection;
		}

		public IEnumerable<Vacancy> List()
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "SELECT * FROM Вакансия";
			OleDbDataReader reader = cmd.ExecuteReader();

			DataTable myDataTable = new DataTable();
			myDataTable.Load(reader);

			List<Vacancy> vacancies = new List<Vacancy>();

			foreach (DataRow vacancyRow in myDataTable.Rows)
			{
				vacancies.Add(RowToVacancy(vacancyRow));
			}

			return vacancies;
		}

		Vacancy RowToVacancy(DataRow row)
		{
			Vacancy newVacancy = new Vacancy(
				repository: this,
				id: Convert.ToInt32(row["Код"]),
				name: row["Название"].ToString());

			return newVacancy;
		}

		public Vacancy GetVacancy(int id)
		{
			throw new NotImplementedException();
		}

		public int UpdateVacancy(Vacancy vacancy)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "UPDATE Вакансия "
				+ "SET "
				+ "Вакансия.Название = '" + vacancy.Name + "' "
				+ "WHERE Вакансия.Код = " + vacancy.Id.ToString();
			var changed = cmd.ExecuteNonQuery();
			//if (changed == 0)
				//throw new Exception();
			return changed;
		}

		public int AddVacancy(Vacancy vacancy)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "INSERT INTO Вакансия (Название) Values('" + vacancy.Name + "')";
			return cmd.ExecuteNonQuery();
		}

		public int DeleteVacancy(int id)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "DELETE * FROM Вакансия WHERE Вакансия.Код=" + id.ToString();
			return cmd.ExecuteNonQuery();
		}
	}
}
