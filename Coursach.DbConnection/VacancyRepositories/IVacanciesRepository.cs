﻿using Coursach.DbConnection.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursach.DbConnection.Repositories
{
	public interface IVacanciesRepository : Listable<Vacancy>
	{
		//GET
		Vacancy GetVacancy(int id);

		//POST
		int UpdateVacancy(Vacancy vacancy);

		//PUT
		int AddVacancy(Vacancy vacancy);

		//DELETE
		int DeleteVacancy(int id);
	}
}
