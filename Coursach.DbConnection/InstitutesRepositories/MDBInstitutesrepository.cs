﻿using Coursach.DbConnection.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;

namespace Coursach.DbConnection.InstitutesRepositories
{
	public class MDBInstitutesrepository : IInstitutesRepository
	{
		DbConnection DbConnection;

		public MDBInstitutesrepository(DbConnection dbConnection)
		{
			this.DbConnection = dbConnection;
		}

		public int Count
		{
			get
			{
				OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
				cmd.CommandText = "SELECT COUNT (*) as Total FROM УчебноеЗаведение";
				OleDbDataReader reader = cmd.ExecuteReader();

				DataTable myDataTable = new DataTable();
				myDataTable.Load(reader);

				return Convert.ToInt32(myDataTable.Rows[0][0]);
			}
		}

		public int AddInstitute(Institute institute)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "INSERT INTO УчебноеЗаведение (Название) Values('" + institute.Name + "')";
			return cmd.ExecuteNonQuery();
		}

		public int DeleteInstitute(int id)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "DELETE * FROM УчебноеЗаведение WHERE УчебноеЗаведение.Код=" + id.ToString();
			return cmd.ExecuteNonQuery();
		}

		public Institute GetInstitute(int id)
		{
			throw new NotImplementedException();
		}

		public IEnumerable<Institute> List()
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "SELECT * FROM УчебноеЗаведение";
			OleDbDataReader reader = cmd.ExecuteReader();

			DataTable myDataTable = new DataTable();
			myDataTable.Load(reader);

			List<Institute> institutes = new List<Institute>();

			foreach (DataRow instituteRow in myDataTable.Rows)
			{
				institutes.Add(RowToInstitute(instituteRow));
			}

			return institutes;
		}

		public int UpdateInstitute(Institute institute)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "UPDATE УчебноеЗаведение "
				+ "SET "
				+ "УчебноеЗаведение.Название = '" + institute.Name + "' "
				+ "WHERE УчебноеЗаведение.Код = " + institute.Id.ToString();
			return cmd.ExecuteNonQuery();
		}

		Institute RowToInstitute(DataRow row)
		{
			Institute newInstitute = new Institute(
				repository: this,
				id: Convert.ToInt32(row["Код"]),
				name: row["Название"].ToString());

			return newInstitute;
		}
	}
}
