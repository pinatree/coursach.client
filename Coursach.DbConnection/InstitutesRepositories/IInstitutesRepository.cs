﻿using Coursach.DbConnection.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursach.DbConnection.InstitutesRepositories
{
	public interface IInstitutesRepository : Listable<Institute>
	{
		//GET
		Institute GetInstitute(int id);

		//POST
		int UpdateInstitute(Institute institute);

		//PUT
		int AddInstitute(Institute institute);

		//DELETE
		int DeleteInstitute(int id);
	}
}
