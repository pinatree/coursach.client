﻿using Coursach.DbConnection.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursach.DbConnection.InterviewRepositories
{
	public interface IInterviewsRepository : Listable<Interview>
	{
		int UpdateInterview(Interview oldInterview, Interview newInterview);

		int AddInterview(Interview interview);

		int DeleteInterview(string passport, string comRegNumber, int vacancyId, DateTime creationDate);
	}
}
