﻿using Coursach.DbConnection.CompanyVacancyRepositories;
using Coursach.DbConnection.Models;
using Coursach.DbConnection.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;

namespace Coursach.DbConnection.InterviewRepositories
{
	public class MDBInterviewsRepository : IInterviewsRepository
	{
		public DbConnection DbConnection;

		public int Count
		{
			get
			{
				OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
				cmd.CommandText = "SELECT COUNT (*) as Total FROM Собеседование";
				OleDbDataReader reader = cmd.ExecuteReader();

				DataTable myDataTable = new DataTable();
				myDataTable.Load(reader);

				return Convert.ToInt32(myDataTable.Rows[0][0]);
			}
		}

		public MDBInterviewsRepository(DbConnection dbConnection)
		{
			this.DbConnection = dbConnection;
		}

		public IEnumerable<Interview> List()
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "SELECT * FROM Собеседование";
			OleDbDataReader reader = cmd.ExecuteReader();

			DataTable myDataTable = new DataTable();
			myDataTable.Load(reader);

			List<Interview> interviews = new List<Interview>();

			foreach (DataRow vacancyRow in myDataTable.Rows)
			{
				interviews.Add(RowToInterview(vacancyRow));
			}

			return interviews;
		}

		public int UpdateInterview(Interview oldInterview, Interview newInterview)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText =
				"UPDATE Собеседование SET Собеседование.Паспорт = '" + newInterview.Passport + "', " +
				"Собеседование.РегНомКомпании = '" + newInterview.CompanyRegistrationNumber + "', " +
				"Собеседование.КодВакансии = " + newInterview.VacancyId + ", " +
				"Собеседование.ДатаСоздания = #" + newInterview.CreationDate.ToString("MM/dd/yyyy HH:mm:ss").Replace('.', '/') + "#, " +
				"Собеседование.ДатаСобеседования = #" + newInterview.InterviewDate.ToString("MM/dd/yyyy HH:mm:ss").Replace('.', '/') + "#, " +
				"Собеседование.Трудоустроен = " + newInterview.Success + " " +
				"WHERE((Собеседование.Паспорт = '" + oldInterview.Passport + "') AND " +
				"(Собеседование.РегНомКомпании = '" + oldInterview.CompanyRegistrationNumber + "') AND " +
				"(Собеседование.КодВакансии = " + oldInterview.VacancyId + ") AND " +
				"(Собеседование.ДатаСоздания = #" + oldInterview.CreationDate.ToString("MM/dd/yyyy HH:mm:ss").Replace('.', '/') + "#))";


			int result = cmd.ExecuteNonQuery();

			if (result == 0)
				throw new Exception();
			return result;			
		}

		public int AddInterview(Interview interview)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "INSERT INTO Собеседование VALUES ('"
				+ interview.Passport + "','"
				+ interview.CompanyRegistrationNumber + "',"
				+ interview.VacancyId + ",'"
				+ interview.CreationDate.ToString().Replace('.', '/') + "','"
				+ interview.InterviewDate.ToString().Replace('.', '/') + "',"
				+ interview.Success.ToString() + ")";

			int changed = cmd.ExecuteNonQuery();

			if (changed == 0)
				throw new Exception();

			return changed;
		}

		public int DeleteInterview(string passport, string comRegNumber, int vacancyId, DateTime creationDate)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "DELETE * FROM Собеседование " +
				"WHERE ((Собеседование.Паспорт = '" + passport + "') AND " +
				"(Собеседование.РегНомКомпании = '" + comRegNumber + "') AND " +
				"(Собеседование.КодВакансии = " + vacancyId + ") AND " +
				"(Собеседование.ДатаСоздания =#" + creationDate.ToString("MM/dd/yyyy HH:mm:ss").Replace('.', '/') + "#));";

			int changed = cmd.ExecuteNonQuery();

			if (changed == 0)
				throw new Exception();

			return changed;
		}

		Interview RowToInterview(DataRow row)
		{
			Interview newInterview = new Interview(
				passport: Convert.ToString(row["Паспорт"]),
				comRegNum: Convert.ToString(row["РегНомКомпании"]),
				vacancyId: Convert.ToInt32(row["КодВакансии"]),
				creationDate: Convert.ToDateTime(row["ДатаСоздания"]),
				interviewDate: Convert.ToDateTime(row["ДатаСобеседования"]),
				success: Convert.ToBoolean(row["Трудоустроен"]));

			MDBCitizensRepository citizenConnection = new MDBCitizensRepository(DbConnection);
			Citizen citizen = citizenConnection.GetCitizen(newInterview.Passport);
			newInterview.Citizen = citizen;

			MDBCompanyVacanciesRepository companyVacanciesRepository = new MDBCompanyVacanciesRepository(DbConnection);
			CompanyVacancy companyVacancy =
				companyVacanciesRepository.GetCompanyVacancy(newInterview.CompanyRegistrationNumber,
				newInterview.VacancyId,
				newInterview.CreationDate);

			newInterview.CompanyVacancy = companyVacancy;

			return newInterview;
		}
	}
}
