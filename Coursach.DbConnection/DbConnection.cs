﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Data;
using Coursach.DbConnection.Models;
using System.Windows.Forms;

namespace Coursach.DbConnection
{
	public class DbConnection
	{
		string connection_string;

		public OleDbConnection myConnection;

		public DbConnection(string path = null, string password = null)
		{
			connection_string = @"Provider=Microsoft.Jet.OLEDB.4.0;" +
				"Data Source=" + path + ";" +
				"Persist Security Info=True;" +
				"Jet OLEDB:Database Password=";

			if (password != null)
				connection_string += (password);

			connection_string += ";";

			myConnection = new OleDbConnection();
			myConnection.ConnectionString = connection_string;
			myConnection.Open();
		}
	}
}
