﻿using System.Collections.Generic;

namespace Coursach.DbConnection
{
	public interface Listable<T>
	{
		IEnumerable<T> List();

		int Count { get; }
	}
}
