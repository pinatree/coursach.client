﻿using Coursach.DbConnection.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;

namespace Coursach.DbConnection.CompanyVacancyRepositories
{
	public class MDBCompanyVacanciesRepository : ICompanyVacanciesRepository
	{
		DbConnection DbConnection;

		public MDBCompanyVacanciesRepository(DbConnection dbConnection)
		{
			this.DbConnection = dbConnection;
		}

		public int Count
		{
			get
			{
				OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
				cmd.CommandText = "SELECT COUNT (*) as Total FROM ВакансияКомпании";
				OleDbDataReader reader = cmd.ExecuteReader();

				DataTable myDataTable = new DataTable();
				myDataTable.Load(reader);

				return Convert.ToInt32(myDataTable.Rows[0][0]);
			}
		}

		public IEnumerable<CompanyVacancy> List()
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText =
				"SELECT ВакансияКомпании.РегНомКомпании, ВакансияКомпании.КодВакансии, " +
				"ВакансияКомпании.ДатаСоздания, ВакансияКомпании.Открыта, Вакансия.Название, Вакансия.Название, " +
				"Вакансия.Название, Компания.Название, Компания.Адрес " +
				"FROM(ВакансияКомпании INNER JOIN Вакансия ON Вакансия.Код = ВакансияКомпании.КодВакансии) " +
				"INNER JOIN Компания ON Компания.РегНомКомпании = ВакансияКомпании.РегНомКомпании ";


			OleDbDataReader reader = cmd.ExecuteReader();

			DataTable myDataTable = new DataTable();
			myDataTable.Load(reader);

			List<CompanyVacancy> companyVacancies = new List<CompanyVacancy>();

			foreach (DataRow companyVacancyRow in myDataTable.Rows)
			{
				companyVacancies.Add(RowToCompanyVacancy(companyVacancyRow));
			}

			return companyVacancies;
		}

		public CompanyVacancy GetCompanyVacancy(string companyRegNumber, int vacancyId, DateTime creationDate)
		{
			//ВНИМАНИЕ! НЕ ДОДЕЛАНО! БРАТЬ ТОЛЬКО ОСНОВНЫЕ ДАННЫЕ, INCLUDED поля отсутствуют!
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText =
			   "SELECT TOP 1 ВакансияКомпании.РегНомКомпании, ВакансияКомпании.КодВакансии, " +
				"ВакансияКомпании.ДатаСоздания, ВакансияКомпании.Открыта, Вакансия.Название, Вакансия.Название, " +
				"Вакансия.Название, Компания.Название, Компания.Адрес " +
				"FROM(ВакансияКомпании INNER JOIN Вакансия ON Вакансия.Код = ВакансияКомпании.КодВакансии) " +
				"INNER JOIN Компания ON Компания.РегНомКомпании = ВакансияКомпании.РегНомКомпании " +
				"WHERE (ВакансияКомпании.РегНомКомпании) = '" + companyRegNumber + "'" +
				"AND (ВакансияКомпании.КодВакансии) = " + vacancyId.ToString();

			OleDbDataReader reader = cmd.ExecuteReader();

			DataTable myDataTable = new DataTable();
			myDataTable.Load(reader);


			CompanyVacancy first = RowToCompanyVacancy(myDataTable.Rows[0]);

			return first;

		}

		public int AddCompanyVacancy(CompanyVacancy companyVacancy)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "INSERT INTO ВакансияКомпании VALUES ('"
				+ companyVacancy.CompanyRegNumber + "',"
				+ companyVacancy.VacancyId + ",'"
				+ companyVacancy.CreationDate.ToString().Replace('.', '/') + "',"
				+ companyVacancy.Opened.ToString() + ")";

			int changed = cmd.ExecuteNonQuery();

			//if (changed == 0)
				//throw new Exception();

			return changed;
		}

		CompanyVacancy RowToCompanyVacancy(DataRow row)
		{
			CompanyVacancy newCompanyVacancy = new CompanyVacancy(
				regNumber: row["РегНомКомпании"].ToString(),
				vacancyId: Convert.ToInt32(row["КодВакансии"]),
				creationDate: Convert.ToDateTime(row["ДатаСоздания"]),
				opened: Convert.ToBoolean(row["Открыта"]));

			Company innerCompany = new Company(
				regNumber: row["РегНомКомпании"].ToString(),
				name: row["Компания.Название"].ToString(),
				address: row["Адрес"].ToString());

			Vacancy innerVacancy = new Vacancy(
				id: Convert.ToInt32(row["КодВакансии"]),
				name: row["Вакансия.Название"].ToString());

			newCompanyVacancy.Company = innerCompany;
			newCompanyVacancy.Vacancy = innerVacancy;

			return newCompanyVacancy;
		}

		public int UpdateCompanyVacancy(CompanyVacancy oldCV, CompanyVacancy newCV)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText =
				"UPDATE ВакансияКомпании SET " +
				"ВакансияКомпании.РегНомКомпании = '" + newCV.CompanyRegNumber + "', " +
				"ВакансияКомпании.КодВакансии = " + newCV.VacancyId + ", " +
				"ВакансияКомпании.ДатаСоздания = #" + newCV.CreationDate.ToString("MM/dd/yyyy HH:mm:ss").Replace('.','/') + "#," +
				"ВакансияКомпании.Открыта = " + newCV.Opened.ToString() + " " +
				"WHERE (ВакансияКомпании.РегНомКомпании = '" + oldCV.CompanyRegNumber + "') AND " +
				"(ВакансияКомпании.КодВакансии = " + oldCV.VacancyId + ") AND " +
				"(ВакансияКомпании.ДатаСоздания = #" + oldCV.CreationDate.ToString("MM/dd/yyyy HH:mm:ss").Replace('.', '/') + "#);";

			int result = cmd.ExecuteNonQuery();

			//if (result == 0)
				//throw new Exception();
			return result;
		}

		public int DeleteCompanyVacancy(string companyRegNumber, int vacancyId, DateTime creationDate)
		{
			OleDbCommand cmd = DbConnection.myConnection.CreateCommand();
			cmd.CommandText = "DELETE * FROM ВакансияКомпании " +
				"WHERE ((ВакансияКомпании.РегНомКомпании = '" + companyRegNumber + "') AND " +
				"(ВакансияКомпании.КодВакансии = " + vacancyId + ") AND " +
				"(ВакансияКомпании.ДатаСоздания =#" + creationDate.ToString("MM/dd/yyyy HH:mm:ss").Replace('.', '/') + "#));";


			int changed = cmd.ExecuteNonQuery();

			//if (changed == 0)
				//throw new Exception();

			return changed;
		}
	}
}
