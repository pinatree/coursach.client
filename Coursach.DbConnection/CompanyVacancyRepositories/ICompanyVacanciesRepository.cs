﻿using Coursach.DbConnection.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursach.DbConnection.CompanyVacancyRepositories
{
	public interface ICompanyVacanciesRepository : Listable<CompanyVacancy>
	{
		CompanyVacancy GetCompanyVacancy(string companyRegNumber, int vacancyId, DateTime creationDate);

		int UpdateCompanyVacancy(CompanyVacancy oldCV, CompanyVacancy newCV);

		int AddCompanyVacancy(CompanyVacancy companyVacancy);

		int DeleteCompanyVacancy(string companyRegNumber, int vacancyId, DateTime creationDate);
	}
}
