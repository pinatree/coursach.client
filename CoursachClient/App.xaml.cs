﻿using System.Windows;

using Autofac;
using Coursach.DbConnection;

using Coursach.DbConnection.Repositories;
using Coursach.DbConnection.CompanyVacancyRepositories;
using Coursach.DbConnection.InterviewRepositories;
using Coursach.DbConnection.DiplomasRepositories;
using Coursach.DbConnection.EducationLevelsRepositories;
using Coursach.DbConnection.GendersRepositories;
using Coursach.DbConnection.InstitutesRepositories;

namespace CoursachClient
{
	public partial class App : Application
	{
		public static DbConnection DatabaseConnection;
		public static IContainer DatabaseDependenciesContainer { get; set; }

		public App()
		{
			var builder = new ContainerBuilder();
		}

		public static void SetupDatabase(string databasePath, string password = null)
		{
			var builder = new ContainerBuilder();

			if(password != null)
				DatabaseConnection = new DbConnection(databasePath, password);
			else
				DatabaseConnection = new DbConnection(databasePath);


			ICitizensRepository citizenRepository = new MDBCitizensRepository(DatabaseConnection);
			ICompaniesRepository companiesRepository = new MDBCompaniesRepository(DatabaseConnection);
			ICompanyVacanciesRepository companyVacanciesRepository = new MDBCompanyVacanciesRepository(DatabaseConnection);
			IVacanciesRepository vacanciesRepository = new MDBVacanciesRepository(DatabaseConnection);
			IInterviewsRepository interviewsRepository = new MDBInterviewsRepository(DatabaseConnection);
			IDiplomasRepository diplomasRepository = new MDBDiplomasRepository(DatabaseConnection);
			IEducationLevelsRepository eduLevelsRepository = new MDBEducationLevelsRepository(DatabaseConnection);
			IGendersRepository gendersRepository = new MDBGendersRepository(DatabaseConnection);
			IInstitutesRepository institutesRepository = new MDBInstitutesrepository(DatabaseConnection);

			builder.RegisterInstance(citizenRepository);
			builder.RegisterInstance(companiesRepository);
			builder.RegisterInstance(companyVacanciesRepository);
			builder.RegisterInstance(vacanciesRepository);
			builder.RegisterInstance(interviewsRepository);
			builder.RegisterInstance(diplomasRepository);
			builder.RegisterInstance(eduLevelsRepository);
			builder.RegisterInstance(gendersRepository);
			builder.RegisterInstance(institutesRepository);

			DatabaseDependenciesContainer = builder.Build();
		}
	}
}
