﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Globalization;
using System.Windows.Data;

using System.Windows.Media;

namespace CoursachClient.Helpers
{
    [ValueConversion(typeof(bool), typeof(SolidColorBrush))]
    public class SuccessToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var correct = (bool)value;

            if (correct)
                return new SolidColorBrush(Colors.LightGreen);
            else
                return new SolidColorBrush(Colors.LightPink);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
