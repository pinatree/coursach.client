﻿using Coursach.DbConnection.DiplomasRepositories;
using Coursach.DbConnection.EducationLevelsRepositories;
using Coursach.DbConnection.InstitutesRepositories;
using Coursach.DbConnection.Models;
using Coursach.DbConnection.Repositories;
using CoursachClient.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;

namespace CoursachClient.ViewModel
{
	public class DiplomaInfoViewModel : INotifyPropertyChanged
	{
		public RelayCommand SaveCommand { get; set; }

		Action CloseCallback;

		private Diploma diploma;
		public Diploma Diploma
		{
			get => diploma;
			set
			{
				if(value != diploma)
				{
					diploma = value;
					NotifyPropertyChanged();
				}
			}
		}

		public DiplomaInfoViewModel(Diploma selected, ICitizensRepository citizensRepository, IInstitutesRepository institutesRepository,
			IEducationLevelsRepository educationLevelsRepository, IDiplomasRepository diplomasRepository, Action closeCallback)
		{
			this.CloseCallback = closeCallback;

			Diploma = selected;

			AvailableEduLevels = educationLevelsRepository.List();
			SelectedEduLevel = AvailableEduLevels.First(x => x.Id == Diploma.EducationLevelId);

			AvailableInstitutes = institutesRepository.List();
			SelectedInstitute = AvailableInstitutes.First(x => x.Id == Diploma.InstituteId);

			
			AvailableCitizens = citizensRepository.List();
			SelectedCitizen = AvailableCitizens.First(x => x.Passport == selected.Passport);


			RegistrationNumber = selected.RegistrationNumber;
			Speciality = selected.Specialty;
			IssueDate = selected.IssueDate;

			SaveCommand = new RelayCommand((x) => Save());
		}

		public IEnumerable<Citizen> AvailableCitizens { get; }

		private Citizen selectedCitizen;
		public Citizen SelectedCitizen
		{
			get => selectedCitizen;
			set
			{
				if (value != selectedCitizen)
				{
					selectedCitizen = value;
					NotifyPropertyChanged();
				}
			}
		}


		public IEnumerable<EducationLevel> AvailableEduLevels { get; }

		private EducationLevel selectedEduLevel;
		public EducationLevel SelectedEduLevel
		{
			get => selectedEduLevel;
			set
			{
				if (value != selectedEduLevel)
				{
					selectedEduLevel = value;
					NotifyPropertyChanged();
				}
			}
		}

		public IEnumerable<Institute> AvailableInstitutes { get; }

		private Institute selectedInstitute;
		public Institute SelectedInstitute
		{
			get => selectedInstitute;
			set
			{
				if (value != selectedInstitute)
				{
					selectedInstitute = value;
					NotifyPropertyChanged();
				}
			}
		}

		public string RegistrationNumber
		{
			get;
			set;
		}

		private bool passpordIsCorrect;
		public bool PasspordIsCorrect
		{
			get => passpordIsCorrect;
			set => passpordIsCorrect = value;
		}

		public string Speciality
		{
			get;
			set;
		}

		public DateTime IssueDate
		{
			get;
			set;
		}

		void Save()
		{
			try
			{
				Diploma.Passport = SelectedCitizen.Passport;
				Diploma.EducationLevelId = SelectedEduLevel.Id;
				Diploma.InstituteId = SelectedInstitute.Id;
				Diploma.IssueDate = IssueDate;
				Diploma.Specialty = Speciality;

				CloseCallback?.Invoke();
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		#region NotifyPropertyChanged

		public event PropertyChangedEventHandler PropertyChanged;

		private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion NotifyPropertyChanged
	}
}
