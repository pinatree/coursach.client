﻿using Coursach.DbConnection.DiplomasRepositories;
using Coursach.DbConnection.EducationLevelsRepositories;
using Coursach.DbConnection.InstitutesRepositories;
using Coursach.DbConnection.Models;
using Coursach.DbConnection.Repositories;
using CoursachClient.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;

namespace CoursachClient.ViewModel
{
	public class DiplomaCreationViewModel : INotifyPropertyChanged
	{
		public IDiplomasRepository DiplomasRepository;

		public RelayCommand SaveCommand { get; set; }

		Action CloseCallback;

		public DiplomaCreationViewModel(ICitizensRepository citizensRepository, IInstitutesRepository institutesRepository,
			IEducationLevelsRepository educationLevelsRepository, IDiplomasRepository diplomasRepository, Action closeCallback)
		{
			this.CloseCallback = closeCallback;

			DiplomasRepository = diplomasRepository;

			AvailableEduLevels = educationLevelsRepository.List();
			SelectedEduLevel = AvailableEduLevels.First();

			AvailableInstitutes = institutesRepository.List();
			SelectedInstitute = AvailableInstitutes.First();

			AvailableCitizens = citizensRepository.List();
			SelectedCitizen = AvailableCitizens.First();

			SaveCommand = new RelayCommand((x) => Save());
		}

		public IEnumerable<Citizen> AvailableCitizens { get; }

		private Citizen selectedCitizen;
		public Citizen SelectedCitizen
		{
			get => selectedCitizen;
			set
			{
				if (value != selectedCitizen)
				{
					selectedCitizen = value;
					NotifyPropertyChanged();
				}
			}
		}


		public IEnumerable<EducationLevel> AvailableEduLevels { get; }

		private EducationLevel selectedEduLevel;
		public EducationLevel SelectedEduLevel
		{
			get => selectedEduLevel;
			set
			{
				if (value != selectedEduLevel)
				{
					selectedEduLevel = value;
					NotifyPropertyChanged();
				}
			}
		}

		public IEnumerable<Institute> AvailableInstitutes { get; }

		private Institute selectedInstitute;
		public Institute SelectedInstitute
		{
			get => selectedInstitute;
			set
			{
				if (value != selectedInstitute)
				{
					selectedInstitute = value;
					NotifyPropertyChanged();
				}
			}
		}

		public string RegistrationNumber
		{
			get;
			set;
		}

		private bool passwordIsCorrect;
		public bool PasswordIsCorrect
		{
			get => passwordIsCorrect;
			set => passwordIsCorrect = value;
		}

		public string Speciality
		{
			get;
			set;
		}

		public DateTime IssueDate
		{
			get;
			set;
		}

		void Save()
		{
			try
			{

				Diploma diploma = new Diploma(
					DiplomasRepository,
					regNumber: RegistrationNumber,
					passport: SelectedCitizen.Passport,
					instititeId: SelectedInstitute.Id,
					eduLevelId: SelectedEduLevel.Id,
					speciality: Speciality,
					issueDate: IssueDate);

				DiplomasRepository.AddDiploma(diploma);
				CloseCallback?.Invoke();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		#region NotifyPropertyChanged

		public event PropertyChangedEventHandler PropertyChanged;

		private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion NotifyPropertyChanged
	}
}