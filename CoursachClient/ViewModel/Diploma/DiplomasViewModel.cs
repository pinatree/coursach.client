﻿using System;
using System.Collections.Generic;

using System.ComponentModel;
using Coursach.DbConnection.Models;
using Coursach.DbConnection.DiplomasRepositories;
using CoursachClient.Helpers;
using CoursachClient.View.Windows.Diploma;
using CoursachClient.ViewModel.Architecture;
using System.Windows.Forms;
using CoursachClient.View.Elements;

namespace CoursachClient.ViewModel
{
    public class DiplomasViewModel : ItemsDemonstrator<Diploma>
    {
        IDiplomasRepository Repository;

        public RelayCommand CreateDiplomaCommand { get; }
        public RelayCommand EditDiplomaCommand { get; }
        public RelayCommand DeleteDiplomaCommand { get; }

        public RelayCommand UpdateRegNumCommand { get; }

        public override IEnumerable<Diploma> Items
        {
            get => Repository.List();
        }

        private Diploma selectedItem;
        public override Diploma SelectedItem
        {
            get => selectedItem;
            set
            {
                if(value != selectedItem)
                {
                    selectedItem = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public DiplomasViewModel(IDiplomasRepository repository)
        {
            this.Repository = repository;
            this.CreateDiplomaCommand = new RelayCommand((x) => CreateDiploma());
            this.EditDiplomaCommand = new RelayCommand((x) => EditDiploma());
            this.DeleteDiplomaCommand = new RelayCommand((x) => DeleteDiploma());
            this.UpdateRegNumCommand = new RelayCommand((x) => UpdateRegNum());
        }


        void CreateDiploma()
        {
            new DiplomaCreation().ShowDialog();
            NotifyPropertyChanged("Items");
        }

        void EditDiploma()
        {
            if (SelectedItem != null)
            {
                new DiplomaInfo(SelectedItem).ShowDialog();
                NotifyPropertyChanged("Items");
            }
        }

        void DeleteDiploma()
        {
            if (SelectedItem == null)
                return;

            DialogResult dialogResult = System.Windows.Forms.MessageBox.Show("Вы уверены, что хотите удалить выбранный диплом?",
                "Вы уверены?", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.No)
                return;

            Repository.DeleteDiploma(SelectedItem.RegistrationNumber);

            NotifyPropertyChanged("Items");
        }

        void UpdateRegNum()
        {
            if (SelectedItem == null)
                return;

            var wind = new InputBox("Новое значение рег. номера", "Пожалуйста, введите новый регистрационный номер");
            wind.ShowDialog();
            if (wind.Success && wind.Result != "")
            {
                try
                {
                    Repository.UpdateRegNumber(SelectedItem.RegistrationNumber, wind.Result);
                    NotifyPropertyChanged("Items");
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                }
            }
        }

        #region NotifyPropertyChanged

        public new event PropertyChangedEventHandler PropertyChanged;

        #endregion NotifyPropertyChanged
    }
}
