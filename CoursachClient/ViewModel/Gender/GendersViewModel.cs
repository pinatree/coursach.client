﻿using Coursach.DbConnection.GendersRepositories;
using Coursach.DbConnection.Models;
using Coursach.DbConnection.Repositories;
using CoursachClient.Helpers;
using CoursachClient.View.Elements;
using CoursachClient.ViewModel.Architecture;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;

namespace CoursachClient.ViewModel
{
	public class GendersViewModel : ItemsDemonstrator<Gender>
	{
        IGendersRepository GendersRepository;

        public RelayCommand CreateGenderCommand { get; set; }
        public RelayCommand DeleteGenderCommand { get; set; }

        public override IEnumerable<Gender> Items
        {
            get => GendersRepository.List();
        }

        private Gender selectedItem;
        public override Gender SelectedItem
        {
            get => selectedItem;
            set
            {
                if (value != selectedItem)
                {
                    selectedItem = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public GendersViewModel(IGendersRepository gendersRepository)
        {
            this.GendersRepository = gendersRepository;

            this.CreateGenderCommand = new RelayCommand((x) => CreateGender());
            this.DeleteGenderCommand = new RelayCommand((x) => DeleteGender());
        }

        void CreateGender()
        {
            var wind = new InputBox("Новый пол", "Пожалуйста, введите название нового пола");
            wind.ShowDialog();
            if(wind.Success && wind.Result != "")
            {
                try
                {
                    GendersRepository.AddGender(new Gender(GendersRepository, -1, wind.Result));
                    NotifyPropertyChanged("Items");
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        void DeleteGender()
        {
            if (SelectedItem == null)
                return;

            string sMessageBoxText = "Вы увререны, что хотите удалить пол " + SelectedItem.Name + "?";
            string sCaption = "Вы уверенность";

            MessageBoxButton btnMessageBox = MessageBoxButton.YesNoCancel;
            MessageBoxImage icnMessageBox = MessageBoxImage.Warning;

            MessageBoxResult rsltMessageBox = MessageBox.Show(sMessageBoxText, sCaption, btnMessageBox, icnMessageBox);

            if (rsltMessageBox != MessageBoxResult.Yes)
                return;
            
            try
            {
                GendersRepository.DeleteGender(SelectedItem.Id);
                NotifyPropertyChanged("Items");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #region NotifyPropertyChanged

        public new event PropertyChangedEventHandler PropertyChanged;

        #endregion NotifyPropertyChanged
    }
}
