﻿using Coursach.DbConnection.CompanyVacancyRepositories;
using Coursach.DbConnection.InterviewRepositories;
using Coursach.DbConnection.Models;
using Coursach.DbConnection.Repositories;
using CoursachClient.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CoursachClient.ViewModel
{
	public class InterviewInfoViewModel : INotifyPropertyChanged
	{
		public RelayCommand SaveCommand { get; set; }

		Action CloseCallback;
		Interview oldInterview;

		ICompanyVacanciesRepository CompanyVacanciesRepository;
		ICitizensRepository CitizensRepository;
		IInterviewsRepository InterviewsRepository;

		public InterviewInfoViewModel(Interview change, ICompanyVacanciesRepository companyVacanciesRepository,
			ICitizensRepository citizensRepository, IInterviewsRepository interviewsRepository, Action closeCallback = null)
		{
			oldInterview = change;

			this.CloseCallback = closeCallback;
			InterviewsRepository = interviewsRepository;
			CompanyVacanciesRepository = companyVacanciesRepository;
			CitizensRepository = citizensRepository;
			SelectedCompanyVacancy = AvailableCompanyVacancies.First(x => (x.VacancyId == change.VacancyId) &&
			(x.CompanyRegNumber == change.CompanyRegistrationNumber) &&
			(x.CreationDate == change.CreationDate));
			SelectedCitizen = AvailableCitizens.First(x => x.Passport == change.Passport);

			InterviewDate = change.InterviewDate;

			Success = change.Success;

			SaveCommand = new RelayCommand((x) => Save());
		}

		public IEnumerable<CompanyVacancy> AvailableCompanyVacancies
		{
			get => CompanyVacanciesRepository.List();
		}

		private CompanyVacancy selectedCompanyVacancy;
		public CompanyVacancy SelectedCompanyVacancy
		{
			get => selectedCompanyVacancy;
			set
			{
				if (value != selectedCompanyVacancy)
				{
					selectedCompanyVacancy = value;
					NotifyPropertyChanged();
				}
			}
		}

		public IEnumerable<Citizen> AvailableCitizens
		{
			get => CitizensRepository.List();
		}

		private Citizen selectedCitizen;
		public Citizen SelectedCitizen
		{
			get => selectedCitizen;
			set
			{
				if (value != selectedCitizen)
				{
					selectedCitizen = value;
					NotifyPropertyChanged();
				}
			}
		}


		private DateTime interviewDate;
		public DateTime InterviewDate
		{
			get => interviewDate;
			set
			{
				if (value != interviewDate)
				{
					interviewDate = value;
					NotifyPropertyChanged();
				}
			}
		}

		private bool success;
		public bool Success
		{
			get => success;
			set
			{
				if (value != success)
				{
					success = value;
					NotifyPropertyChanged();
				}
			}
		}


		void Save()
		{
			if (SelectedCitizen == null || SelectedCompanyVacancy == null)
				return;

			
			//try
			{
				Interview interview = new Interview(SelectedCitizen.Passport,
					SelectedCompanyVacancy.CompanyRegNumber,
					SelectedCompanyVacancy.VacancyId,
					SelectedCompanyVacancy.CreationDate,
					interviewDate: InterviewDate,
					success: Success);

				InterviewsRepository.UpdateInterview(oldInterview, interview);
				CloseCallback?.Invoke();
			}
			//catch (Exception ex)
			{
			//	MessageBox.Show(ex.Message);
			}
		}

		#region NotifyPropertyChanged

		public event PropertyChangedEventHandler PropertyChanged;

		private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion NotifyPropertyChanged

	}
}