﻿using Coursach.DbConnection.InterviewRepositories;
using Coursach.DbConnection.Models;
using CoursachClient.Helpers;
using CoursachClient.View.Reports;
using CoursachClient.View.Windows.Interview;
using CoursachClient.ViewModel.Architecture;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace CoursachClient.ViewModel
{
    public class InterviewViewModel : ItemsDemonstrator<Interview>
    {
        IInterviewsRepository Repository;

        public RelayCommand CreateInterviewCommand { get; set; }
        public RelayCommand DeleteInterviewCommand { get; set; }
        public RelayCommand OpenInterviewCommand { get; set; }

        public RelayCommand OpenInterviewsReportCommand { get; set; }

        public override IEnumerable<Interview> Items
        {
            get => Repository.List();
        }

        private Interview selectedItem;
        public override Interview SelectedItem
        {
            get => selectedItem;
            set
            {
                if(value != selectedItem)
                {
                    selectedItem = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public InterviewViewModel(IInterviewsRepository interviewsRepository)
        {
            this.Repository = interviewsRepository;
            this.CreateInterviewCommand = new RelayCommand((x) => CreateInterview());
            this.DeleteInterviewCommand = new RelayCommand((x) => DeleteInterview());
            this.OpenInterviewCommand = new RelayCommand((x) => OpenInterview());

            OpenInterviewsReportCommand = new RelayCommand((x) =>
            {
                new CitizenInterviewsReportWindow().Show();
            });
        }

        void CreateInterview()
        {
            new InterviewCreation().ShowDialog();
            NotifyPropertyChanged("Items");
        }

        void OpenInterview()
        {
            if (SelectedItem == null)
                return;

            new InterviewInfo(SelectedItem).ShowDialog();
            NotifyPropertyChanged("Items");
        }

        void DeleteInterview()
        {
            if (SelectedItem == null)
                return;

            DialogResult dialogResult = System.Windows.Forms.MessageBox.Show("Вы уверены, что хотите удалить выбранную вакансию?",
                "Вы уверены?", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.No)
                return;

            Repository.DeleteInterview(SelectedItem.Passport, SelectedItem.CompanyRegistrationNumber, SelectedItem.VacancyId, SelectedItem.CreationDate);
            NotifyPropertyChanged("Items");
        }

        #region NotifyPropertyChanged

        public new event PropertyChangedEventHandler PropertyChanged;

        #endregion NotifyPropertyChanged
    }
}
