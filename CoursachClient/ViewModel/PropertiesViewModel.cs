﻿using System;
using System.Collections.Generic;
using System.Windows;
using CoursachClient.AppConfiguration.Fonts;
using CoursachClient.AppConfiguration.Themes;
using CoursachClient.Helpers;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace CoursachClient.ViewModel
{
	public interface IPropertiesViewModel : INotifyPropertyChanged
	{
		RelayCommand SaveCommand { get; set; }

		List<ThemeInfo> AvailableThemes { get; }
		ThemeInfo SelectedTheme { get; set; }

		List<int> AvailableFontSizes { get; }
		int SelectedFontSize { get; set; }
	}

	public class PropertiesViewModel : IPropertiesViewModel
	{
		public RelayCommand SaveCommand { get; set; }


		public List<ThemeInfo> AvailableThemes
		{
			get;
			set;
		} = ThemeConfiguration.Instance.AvailableThemes;

		private ThemeInfo selectedTheme;
		public ThemeInfo SelectedTheme
		{
			get => selectedTheme;
			set
			{
				if (value != selectedTheme)
				{
					selectedTheme = value;
					NotifyPropertyChanged();
				}
			}
		}


		private List<int> availableFontSizes = FontConfiguration.Instance.AvailableFontSizes;
		public List<int> AvailableFontSizes
		{
			get => availableFontSizes;
			set
			{
				if(value != availableFontSizes)
				{
					availableFontSizes = value;
					NotifyPropertyChanged();
				}
			}
		}

		private int selectedFontSize = FontConfiguration.Instance.FontSize;
		public int SelectedFontSize
		{
			get => selectedFontSize;
			set
			{
				if (value != selectedFontSize)
				{
					selectedFontSize = value;
					NotifyPropertyChanged();
				}
			}
		}

		public PropertiesViewModel()
		{
			SelectedTheme = ThemeConfiguration.Instance.SelectedTheme;
			SaveCommand = new RelayCommand((x) => Save());

		}

		void Save()
		{
			FontConfiguration.Instance.FontSize = this.SelectedFontSize;
			ThemeConfiguration.Instance.SelectedTheme = this.SelectedTheme;
			
			MessageBox.Show("Сохранено!");
		}

		public event PropertyChangedEventHandler PropertyChanged;

		private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
