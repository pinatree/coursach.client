﻿using Coursach.DbConnection.CompanyVacancyRepositories;
using Coursach.DbConnection.Models;
using Coursach.DbConnection.Repositories;
using CoursachClient.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;

namespace CoursachClient.ViewModel
{
    public class CompanyVacancyInfoViewModel : INotifyPropertyChanged
    {
        ICompanyVacanciesRepository CompanyVacanciesRepository;

        public RelayCommand SaveCommand { get; set; }
        public CompanyVacancy Old { get; set; }

        Action CloseCallBack;

        public CompanyVacancyInfoViewModel(CompanyVacancy change, ICompaniesRepository companiesRepository,
            IVacanciesRepository vacanciesRepository, ICompanyVacanciesRepository companyVacanciesRepository, Action closeCallback)
        {
            this.CloseCallBack = closeCallback;

            Old = change;

            this.CompanyVacanciesRepository = companyVacanciesRepository;

            this.AvailableCompanies = companiesRepository.List();
            SelectedCompany = AvailableCompanies.First(x => x.CompanyRegistrationNumber == change.CompanyRegNumber);

            this.AvailableVacancies = vacanciesRepository.List();
            SelectedVacancy = AvailableVacancies.First(x => x.Id == change.Vacancy.Id);

            IsOpened = change.Opened;
            CreationDate = change.CreationDate;

            SaveCommand = new RelayCommand((x) => Save());
        }


        public IEnumerable<Company> AvailableCompanies { get; }
        private Company selectedCompany;
        public Company SelectedCompany
        {
            get => selectedCompany;
            set
            {
                if(value != selectedCompany)
                {
                    selectedCompany = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public IEnumerable<Vacancy> AvailableVacancies { get; }
        private Vacancy selectedVacancy;
        public Vacancy SelectedVacancy
        {
            get => selectedVacancy;
            set
            {
                if(value != selectedVacancy)
                {
                    selectedVacancy = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public DateTime CreationDate { get; set; } = DateTime.Now;

        public bool IsOpened { get; set; }

        void Save()
        {
            try
            {
                CompanyVacancy newCV =
                    new CompanyVacancy(SelectedCompany.CompanyRegistrationNumber, SelectedVacancy.Id, CreationDate, IsOpened);

                CompanyVacanciesRepository.UpdateCompanyVacancy(Old, newCV);

                CloseCallBack?.Invoke();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion NotifyPropertyChanged
    }
}
