﻿using System.Collections.Generic;
using System.ComponentModel;
using Coursach.DbConnection.CompanyVacancyRepositories;
using Coursach.DbConnection.Models;
using CoursachClient.Helpers;
using CoursachClient.View.Windows.CompanyVacancy;
using CoursachClient.ViewModel.Architecture;
using System.Windows.Forms;
using CoursachClient.View.Reports;

namespace CoursachClient.ViewModel
{
    public class CompanyVacanciesViewModel : ItemsDemonstrator<CompanyVacancy>
    {
        ICompanyVacanciesRepository Repository;

        public RelayCommand CreateCompanyVacancyCommand { get; }
        public RelayCommand EditCompanyVacancyCommand { get; }
        public RelayCommand DeleteCompanyVacancyCommand { get; }

        public RelayCommand OpenedVacanciesReportCommand { get; }

        public override IEnumerable<CompanyVacancy> Items
        {
            get => Repository.List();
        }

        private CompanyVacancy selectedItem;
        public override CompanyVacancy SelectedItem
        {
            get => selectedItem;
            set
            {
                if(value != selectedItem)
                {
                    selectedItem = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public CompanyVacanciesViewModel(ICompanyVacanciesRepository repository)
        {
            this.Repository = repository;
            CreateCompanyVacancyCommand = new RelayCommand((x) => CreateCompanyVacancy());
            EditCompanyVacancyCommand = new RelayCommand((x) => EditCompanyVacancy());
            DeleteCompanyVacancyCommand = new RelayCommand((x) => DeleteCompanyVacancy());

            OpenedVacanciesReportCommand = new RelayCommand((x) =>
            {
                new OpenedCompanyVacanciesReportWindow().ShowDialog();
            });
        }

        void CreateCompanyVacancy()
        {
            new CompanyVacancyCreation().ShowDialog();
            NotifyPropertyChanged("Items");
        }
        void EditCompanyVacancy()
        {
            if (SelectedItem != null)
            {
                new CompanyVacancyInfo(SelectedItem).ShowDialog();
                NotifyPropertyChanged("Items");
            }
        }
        void DeleteCompanyVacancy()
        {
            if (SelectedItem == null)
                return;

            DialogResult dialogResult = System.Windows.Forms.MessageBox.Show("Вы уверены, что хотите удалить выбранное собеседование?",
                "Вы уверены?", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.No)
                return;

            Repository.DeleteCompanyVacancy(SelectedItem.CompanyRegNumber, SelectedItem.VacancyId, SelectedItem.CreationDate);
            NotifyPropertyChanged("Items");
        }

        #region NotifyPropertyChanged

        public new event PropertyChangedEventHandler PropertyChanged;

        #endregion NotifyPropertyChanged
    }
}
