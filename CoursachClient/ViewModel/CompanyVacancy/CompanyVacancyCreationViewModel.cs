﻿using Coursach.DbConnection.CompanyVacancyRepositories;
using Coursach.DbConnection.Models;
using Coursach.DbConnection.Repositories;
using CoursachClient.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;

namespace CoursachClient.ViewModel
{
    public class CompanyVacancyCreationViewModel : INotifyPropertyChanged
    {
        ICompanyVacanciesRepository CompanyVacancyRepository;

        public RelayCommand SaveCommand { get; set; }

        Action CloseCallBack;

        public CompanyVacancyCreationViewModel(ICompanyVacanciesRepository companyVacancyRepository,
            ICompaniesRepository companiesRepository,
            IVacanciesRepository vacanciesRepository,
            Action closeCallBack)
        {
            this.CloseCallBack = closeCallBack;
            this.CompanyVacancyRepository = companyVacancyRepository;

            this.AvailableCompanies = companiesRepository.List();
            SelectedCompany = AvailableCompanies.First();

            this.AvailableVacancies = vacanciesRepository.List();
            SelectedVacancy = AvailableVacancies.First();

            SaveCommand = new RelayCommand((x) => Save());
        }


        public IEnumerable<Company> AvailableCompanies { get; }

        private Company selectedCompany;
        public Company SelectedCompany
        {
            get => selectedCompany;
            set
            {
                if(value != selectedCompany)
                {
                    selectedCompany = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public IEnumerable<Vacancy> AvailableVacancies { get; }

        private Vacancy selectedVacancy;
        public Vacancy SelectedVacancy
        {
            get => selectedVacancy;
            set
            {
                if(value != selectedVacancy)
                {
                    selectedVacancy = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public DateTime CreationDate { get; set; } = DateTime.Now;

        public bool IsOpened { get; set; }

        void Save()
        {
            try
            {
                CompanyVacancy companyVacancy = new CompanyVacancy(
                    regNumber: SelectedCompany.CompanyRegistrationNumber,
                    vacancyId: SelectedVacancy.Id,
                    creationDate: CreationDate,
                    opened: IsOpened);

                CompanyVacancyRepository.AddCompanyVacancy(companyVacancy);
                CloseCallBack?.Invoke();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion NotifyPropertyChanged
    }
}
