﻿using System;
using System.Collections.Generic;
using System.Windows;
using CoursachClient.Helpers;
using System.ComponentModel;

using Coursach.DbConnection.EducationLevelsRepositories;

using Coursach.DbConnection.Models;
using CoursachClient.View.Elements;
using CoursachClient.ViewModel.Architecture;

namespace CoursachClient.ViewModel
{
    public class EducationLevelsViewModel : ItemsDemonstrator<EducationLevel>
    {
        IEducationLevelsRepository Repository;

        public RelayCommand AddEduLevelCommand { get; set; }
        public RelayCommand DeleteEduLevelCommand { get; set; }

        public override IEnumerable<EducationLevel> Items
        {
            get => Repository.List();
        }

        private EducationLevel selectedItem;
        public override EducationLevel SelectedItem
        {
            get => selectedItem;
            set
            {
                if(value != selectedItem)
                {
                    selectedItem = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public EducationLevelsViewModel(IEducationLevelsRepository repository)
        {
            this.Repository = repository;

            AddEduLevelCommand = new RelayCommand((x) => AddEduLevel());
            DeleteEduLevelCommand = new RelayCommand((x) => DeleteEduLevel());
        }

        void AddEduLevel()
        {
            var wind = new InputBox("Новое уровень образования", "Пожалуйста, введите название");
            wind.ShowDialog();
            if (wind.Success && wind.Result != "")
            {
                try
                {
                    Repository.AddEducationLevel(new EducationLevel(Repository, -1, wind.Result));
                    NotifyPropertyChanged("Items");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        void DeleteEduLevel()
        {
            if (SelectedItem == null)
                return;

            string sMessageBoxText = "Вы увререны, что хотите удалить " + SelectedItem.Name + "?";
            string sCaption = "Вы уверенны?";

            MessageBoxButton btnMessageBox = MessageBoxButton.YesNoCancel;
            MessageBoxImage icnMessageBox = MessageBoxImage.Warning;

            MessageBoxResult rsltMessageBox = MessageBox.Show(sMessageBoxText, sCaption, btnMessageBox, icnMessageBox);

            if (rsltMessageBox != MessageBoxResult.Yes)
                return;

            try
            {
                Repository.DeleteEducationLevel(SelectedItem.Id);
                NotifyPropertyChanged("Items");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        #region NotifyPropertyChanged

        public new event PropertyChangedEventHandler PropertyChanged;

        #endregion NotifyPropertyChanged
    }
}
