﻿using CoursachClient.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace CoursachClient.ViewModel
{
	public interface IFullHeaderHandler
	{
        RelayCommand Hide { get; set; }
        RelayCommand Resize { get; set; }
        RelayCommand Close { get; set; }
        RelayCommand DragMove { get; set; }

        string WindowCaption { get; set; }

        WindowState CurrentState { get; set; }
    }
}
