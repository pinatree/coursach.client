﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CoursachClient.ViewModel.Architecture
{
	public abstract class ItemsDemonstrator<T> : INotifyPropertyChanged
	{
        public abstract IEnumerable<T> Items { get; }
        public abstract T SelectedItem { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
