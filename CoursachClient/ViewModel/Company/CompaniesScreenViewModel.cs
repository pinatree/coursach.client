﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Autofac;
using Coursach.DbConnection.Repositories;
using Coursach.DbConnection.Models;
using CoursachClient.Helpers;
using CoursachClient.View.Windows.Company;
using CoursachClient.ViewModel.Architecture;
using System.Windows.Forms;
using CoursachClient.View.Elements;

namespace CoursachClient.ViewModel
{
	public class CompaniesScreenViewModel : ItemsDemonstrator<Company>
	{
        ICompaniesRepository CompaniesRepository;

        public RelayCommand CreateCompanyCommand { get; }
        public RelayCommand DeleteCompanyCommand { get; }

        public RelayCommand UpdateRegNumberCommand { get; }

        public override IEnumerable<Company> Items
        {
            get => CompaniesRepository.List();
        }

        private Company selectedItem;
        public override Company SelectedItem
        {
            get => selectedItem;
            set
            {
                if (value != selectedItem)
                {
                    selectedItem = value;
                    NotifyPropertyChanged();
                }
            }
        }


        public CompaniesScreenViewModel(ICompaniesRepository companiesRepository)
        {
            this.CompaniesRepository = companiesRepository;
            this.CreateCompanyCommand = new RelayCommand((x) => CreateCompany());
            this.DeleteCompanyCommand = new RelayCommand((x) => DeleteCompany());
            this.UpdateRegNumberCommand = new RelayCommand((x) => UpdateRegNumber());
        }

        void CreateCompany()
        {
            new CreateCompany().ShowDialog();
            NotifyPropertyChanged("Items");
        }

        void DeleteCompany()
        {
            if (SelectedItem == null)
                return;

            DialogResult dialogResult = MessageBox.Show("Вы уверены, что хотите удалить компанию " + SelectedItem.Name + "?",
                "Вы уверены?", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.No)
                return;

            CompaniesRepository.DeleteCompany(SelectedItem.CompanyRegistrationNumber);

            NotifyPropertyChanged("Items");
        }

        void UpdateRegNumber()
        {
            if (SelectedItem == null)
                return;

            var wind = new InputBox("Новое значение рег. номера", "Пожалуйста, введите новый регистрационный номер");
            wind.ShowDialog();
            if (wind.Success && wind.Result != "")
            {
                try
                {
                    CompaniesRepository.UpdateRegistrationNumber(SelectedItem.CompanyRegistrationNumber, wind.Result);
                    NotifyPropertyChanged("Items");
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message);
                }
            }
        }

        #region NotifyPropertyChanged

        public new event PropertyChangedEventHandler PropertyChanged;

        #endregion NotifyPropertyChanged

        public static CompaniesScreenViewModel GetAutofacInjected()
        {
            ICompaniesRepository injectRepos = App.DatabaseDependenciesContainer.Resolve<ICompaniesRepository>();

            return new CompaniesScreenViewModel(injectRepos);
        }
    }
}
