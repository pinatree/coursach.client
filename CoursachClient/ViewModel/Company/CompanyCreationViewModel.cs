﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using Coursach.DbConnection.Models;
using Coursach.DbConnection.Repositories;
using CoursachClient.Helpers;

namespace CoursachClient.ViewModel
{
	public class CompanyCreationViewModel : INotifyPropertyChanged
	{
		ICompaniesRepository Repository;
		Action CloseCallback;

		public RelayCommand SaveCommand { get; set; }

		public Company NewCompany { get; set; } = new Company("", "", "");

		public CompanyCreationViewModel(ICompaniesRepository companiesRepository, Action closeCallback)
		{
			this.Repository = companiesRepository;
			this.CloseCallback = closeCallback;

			this.SaveCommand = new RelayCommand((x) => Save());
		}

		void Save()
		{
			try
			{
				Repository.AddCompany(NewCompany);
				CloseCallback?.Invoke();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		#region NotifyPropertyChanged

		public event PropertyChangedEventHandler PropertyChanged;

		private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion NotifyPropertyChanged
	}
}
