﻿using Coursach.DbConnection.Models;
using Coursach.DbConnection.Repositories;
using CoursachClient.Helpers;
using CoursachClient.View.Elements;
using CoursachClient.ViewModel.Architecture;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;

namespace CoursachClient.ViewModel
{
	public class VacanciesViewModel : ItemsDemonstrator<Vacancy>
	{
        IVacanciesRepository VacanciesRepository;

        public RelayCommand AddVacancyCommand { get; set; }
        public RelayCommand DeleteVacancyCommand { get; set; }

        public override IEnumerable<Vacancy> Items
        {
            get => VacanciesRepository.List();
        }

        private Vacancy selectedItem;
        public override Vacancy SelectedItem
        {
            get => selectedItem;
            set
            {
                if(value != selectedItem)
                {
                    selectedItem = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public VacanciesViewModel(IVacanciesRepository vacanciesRepository)
        {
            this.VacanciesRepository = vacanciesRepository;
            this.AddVacancyCommand = new RelayCommand((x) => AddVacancy());
            this.DeleteVacancyCommand = new RelayCommand((x) => DeleteVacancy());
        }

        void AddVacancy()
        {
            var wind = new InputBox("Новая специальность", "Пожалуйста, введите название");
            wind.ShowDialog();
            if (wind.Success && wind.Result != "")
            {
                try
                {
                    VacanciesRepository.AddVacancy(new Vacancy(VacanciesRepository, -1, wind.Result));
                    NotifyPropertyChanged("Items");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        void DeleteVacancy()
        {
            if (SelectedItem == null)
                return;

            string sMessageBoxText = "Вы увререны, что хотите удалить специальность " + SelectedItem.Name + "?";
            string sCaption = "Вы уверенны?";

            MessageBoxButton btnMessageBox = MessageBoxButton.YesNoCancel;
            MessageBoxImage icnMessageBox = MessageBoxImage.Warning;

            MessageBoxResult rsltMessageBox = MessageBox.Show(sMessageBoxText, sCaption, btnMessageBox, icnMessageBox);

            if (rsltMessageBox != MessageBoxResult.Yes)
                return;

            try
            {
                VacanciesRepository.DeleteVacancy(SelectedItem.Id);
                NotifyPropertyChanged("Items");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        #region NotifyPropertyChanged

        public new event PropertyChangedEventHandler PropertyChanged;

        #endregion NotifyPropertyChanged
    }
}
