﻿using System.ComponentModel;
using CoursachClient.Helpers;

namespace CoursachClient.ViewModel.Authentication
{
	public interface IAuthenticationViewModel : IShortHeaderHandler, INotifyPropertyChanged
	{
		RelayCommand OpenPropertiesCommand { get; }
		RelayCommand AuthenticateCommand { get; }

		string Password { get; set; }
		bool Remember { get; set; }
	}
}
