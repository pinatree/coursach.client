﻿using System;
using System.Windows;
using CoursachClient.Helpers;
using CoursachClient.View.Windows;

using System.ComponentModel;
using System.Runtime.CompilerServices;

using Microsoft.Win32;
using System.IO;
using CoursachClient.AppConfiguration.Database;

namespace CoursachClient.ViewModel.Authentication
{
	public class AuthenticationViewModel : IAuthenticationViewModel
	{
		#region HeaderProperties

		public RelayCommand Close { get; set; }
		public RelayCommand DragMove { get; set; }

		public string WindowCaption { get; set; } = "Вход в систему";

		private WindowState currentState = WindowState.Normal;
		public WindowState CurrentState
		{
			get => currentState;
			set
			{
				if (value != currentState)
				{
					currentState = value;
					NotifyPropertyChanged();
				}
			}
		}

		#endregion HeaderProperties

		const string RELATIVE_PATH = "./Cursovaya_db.mdb";

		public RelayCommand OpenPropertiesCommand { get; }
		public RelayCommand AuthenticateCommand { get; }

		public RelayCommand SearchLocationCommand { get; }

		private string password;
		public string Password
		{
			get => password;
			set => password = value;
		}

		public bool Remember { get; set; } = true;

		private bool useRelative;
		public bool UseRelative
		{
			get => useRelative;
			set
			{
				if (value != useRelative)
				{
					useRelative = value;

					NotifyPropertyChanged();
				}
			}
		}

		private string dbLocation;
		public string DBLocation
		{
			get => dbLocation;
			set
			{
				if (value != dbLocation)
				{
					dbLocation = value;
					NotifyPropertyChanged();
				}
			}
		}

		Action _closeCallback;
		Action _dragMoveCallback;

		public AuthenticationViewModel(Action сloseCallback, Action dragMoveCallback)
		{
			UseRelative = DBConfiguration.Instance.UseRelativePath;
			DBLocation = DBConfiguration.Instance.DBLocation;
			Password = DBConfiguration.Instance.DBPassword;

			this._closeCallback = сloseCallback;
			this._dragMoveCallback = dragMoveCallback;

			Close = new RelayCommand(x => _closeCallback?.Invoke());

			DragMove = new RelayCommand(x => _dragMoveCallback?.Invoke());

			Close = new RelayCommand((x) =>
			{
				if (Remember)
				{
					DBConfiguration.Instance.DBLocation = DBLocation;
					DBConfiguration.Instance.DBPassword = Password;
					DBConfiguration.Instance.UseRelativePath = UseRelative;
				}

				CloseWindow();
			});

			OpenPropertiesCommand = new RelayCommand((x) => OpenProperties());
			AuthenticateCommand = new RelayCommand((x) => Autneiticate());
			SearchLocationCommand = new RelayCommand((x) => SearchDatabase());
		}

		void OpenProperties() => new PropertiesWind().ShowDialog();

		void Autneiticate() => SwitchToMainWindow();

		void SearchDatabase()
		{
			var opf = new OpenFileDialog();
			opf.Filter = "mdb (*.mdb)|*.mdb";
			var dialogResult = opf.ShowDialog();

			if (!dialogResult.HasValue)
				return;

			if (opf.FileName == "")
				return;

			DBLocation = opf.FileName;
		}

		void CloseWindow() => _closeCallback.Invoke();

		void SwitchToMainWindow()
		{
			var dbLocation = DBConfiguration.Instance.DBLocation;

			try
			{
				string path;

				if (UseRelative)
				{
					path = RELATIVE_PATH;

					bool exists = File.Exists(path);

					if (!exists)
					{
						MessageBox.Show("Требуемый файл в директории исполняемого файла отсутствует!");
						return;
					}
				}
				else
				{
					path = DBLocation;

					bool exists = File.Exists(path);

					if (!exists)
					{
						MessageBox.Show("Указанного файла .mdb не существует!");
						return;
					}
				}

				if (Password != "")
					App.SetupDatabase(path, Password);
				else
					App.SetupDatabase(path);

			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
				return;
			}

			if (Remember)
			{
				DBConfiguration.Instance.DBLocation = DBLocation;
				DBConfiguration.Instance.DBPassword = Password;
				DBConfiguration.Instance.UseRelativePath = UseRelative;
			}

			new MainWindow().Show();
			_closeCallback.Invoke();

		}

		public event PropertyChangedEventHandler PropertyChanged;

		private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}