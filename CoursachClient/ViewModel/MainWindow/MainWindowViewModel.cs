﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

using CoursachClient.Helpers;
using CoursachClient.View.Pages;

using System.ComponentModel;
using System.Runtime.CompilerServices;
using CoursachClient.View.Windows;

namespace CoursachClient.ViewModel
{
    public class MainWindowViewModel : IFullHeaderHandler, INotifyPropertyChanged
    {
        #region HeaderProperties

        public RelayCommand Hide { get; set; }
        public RelayCommand Resize { get; set; }
        public RelayCommand Close { get; set; }
        public RelayCommand DragMove { get; set; }

        public string WindowCaption { get; set; } = "Main menu";

        private WindowState currentState = WindowState.Normal;
        public WindowState CurrentState
        {
            get => currentState;
            set
            {
                if (value != currentState)
                {
                    currentState = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion HeaderProperties

        private UserControl selectedScreen = null;
        public UserControl SelectedScreen
        {
            get => selectedScreen;
            set
            {
                if (value != selectedScreen)
                {
                    selectedScreen = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public List<TabSelector> Screens { get; private set; } = new List<TabSelector>();

        private Action closeCallback;
        private Action dragMoveCallback;

        public MainWindowViewModel(Action _closeCallback = null, Action _dragMoveCallback = null)
        {
            this.closeCallback = _closeCallback;
            this.dragMoveCallback = _dragMoveCallback;

            Hide = new RelayCommand(x => { CurrentState = WindowState.Minimized; });

            Resize = new RelayCommand((x) =>
            CurrentState = (CurrentState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized));

            Close = new RelayCommand(x => closeCallback?.Invoke());

            DragMove = new RelayCommand(x => dragMoveCallback?.Invoke());

            Screens = new List<TabSelector>()
            {
                new ReCreateTabSelector<CitizensScreen>("Граждане", SelectPaginator),
                new ReCreateTabSelector<CompaniesScreen>("Компании",  SelectPaginator),
                new ReCreateTabSelector<CompanyVacanciesScreen>("Вакансии",  SelectPaginator),
                new StableTabSelector<VacanciesScreen>("Специальности", null,  SelectPaginator),
                new ReCreateTabSelector<InterviewsScreen>("Собеседования",  SelectPaginator),
                new ReCreateTabSelector<DiplomasScreen>("Дипломы",  SelectPaginator),
                new StableTabSelector<GendersScreen>("Пол", null, SelectPaginator),
                new StableTabSelector<InstitutesScreen>("Уч. заведения", null,  SelectPaginator),
                new StableTabSelector<EducationLevelsScreen>("Уровни образования", null,  SelectPaginator),
                new ActionSelector("Properties", () => OpenProperties())
            };
        }

        void SelectPaginator(UserControl showControl)
        {
            SelectedScreen = showControl;
        }

        void OpenProperties()
        {
            new PropertiesWind().ShowDialog();
        }

        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion NotifyPropertyChanged
    }

    //Класс вкладки, позволяющей что то выбрать в меню
    public abstract class TabSelector : INotifyPropertyChanged
    {
        public string Caption { get; set; }

        public RelayCommand Select { get; set; }

        public TabSelector(string caption)
        {
            this.Caption = caption;
        }

        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion NotifyPropertyChanged
    }

    //Класс, реагирующий простым действием Action
    public class ActionSelector : TabSelector
    {
        public ActionSelector(string caption, Action onInvoke) : base(caption)
        {
            this.Select = new RelayCommand(x => onInvoke?.Invoke());
        }
    }

    //Класс, создающий каждый раз новый экземпляр отображаемого объекта и передающий его
    public class ReCreateTabSelector<T> : TabSelector where T : UserControl, new()
    {
        public ReCreateTabSelector(string caption, Action<UserControl> onSelect = null) : base(caption)
        {
            Select = new RelayCommand(x => onSelect?.Invoke(new T()));
        }
    }

    //Класс, использующий один единственный экземпляр отображаемого объекта и передающий его
    public class StableTabSelector<T> : TabSelector where T : UserControl, new()
    {
        UserControl PageInstance = null;

        public StableTabSelector(string caption, UserControl instance = null, Action<UserControl> onSelect = null) : base(caption)
        {
            if (instance != null)
                PageInstance = instance;
            else
                PageInstance = new T();

            Select = new RelayCommand(x => onSelect?.Invoke(PageInstance));
        }
    }
}
