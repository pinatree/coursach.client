﻿using Coursach.DbConnection.InstitutesRepositories;
using Coursach.DbConnection.Models;
using CoursachClient.Helpers;
using CoursachClient.View.Elements;
using CoursachClient.ViewModel.Architecture;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;

namespace CoursachClient.ViewModel
{
	public class InstitutesViewModel : ItemsDemonstrator<Institute>
	{
        IInstitutesRepository InstitutesRepository;

        public RelayCommand AddInstituteCommand { get; }
        public RelayCommand DeleteInstituteCommand { get; }

        public override IEnumerable<Institute> Items
        {
            get => InstitutesRepository.List();
        }

        private Institute selectedItem;
        public override Institute SelectedItem
        {
            get => selectedItem;
            set
            {
                if(value != selectedItem)
                {
                    selectedItem = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public InstitutesViewModel(IInstitutesRepository institutesRepository)
        {
            this.InstitutesRepository = institutesRepository;
            this.AddInstituteCommand = new RelayCommand((x) => AddInstitute());
            this.DeleteInstituteCommand = new RelayCommand((x) => DeleteInstitute());
        }

        void AddInstitute()
        {
            var wind = new InputBox("Новое учебное заведение", "Пожалуйста, введите название");
            wind.ShowDialog();
            if (wind.Success && wind.Result != "")
            {
                try
                {
                    InstitutesRepository.AddInstitute(new Institute(InstitutesRepository, -1, wind.Result));
                    NotifyPropertyChanged("Items");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        void DeleteInstitute()
        {
            if (SelectedItem == null)
                return;

            string sMessageBoxText = "Вы увререны, что хотите удалить пол " + SelectedItem.Name + "?";
            string sCaption = "Вы уверенны?";

            MessageBoxButton btnMessageBox = MessageBoxButton.YesNoCancel;
            MessageBoxImage icnMessageBox = MessageBoxImage.Warning;

            MessageBoxResult rsltMessageBox = MessageBox.Show(sMessageBoxText, sCaption, btnMessageBox, icnMessageBox);

            if (rsltMessageBox != MessageBoxResult.Yes)
                return;

            try
            {
                InstitutesRepository.DeleteInstitute(SelectedItem.Id);
                NotifyPropertyChanged("Items");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        #region NotifyPropertyChanged

        public new event PropertyChangedEventHandler PropertyChanged;

        #endregion NotifyPropertyChanged
    }
}
