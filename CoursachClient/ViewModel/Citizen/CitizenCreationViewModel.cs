﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using Autofac;
using Coursach.DbConnection.GendersRepositories;
using Coursach.DbConnection.Models;
using Coursach.DbConnection.Repositories;
using CoursachClient.Helpers;

namespace CoursachClient.ViewModel
{
	public class CitizenCreationViewModel : INotifyPropertyChanged
	{
		ICitizensRepository Repository;
		Action CloseCallback;

		public RelayCommand SaveCommand { get; set; }

		public IEnumerable<Gender> AvailableGenders { get; }
		public Gender SelectedGender { get; set; }

		public Citizen NewCitizen { get; set; }

		public CitizenCreationViewModel(ICitizensRepository citizensRepository, Action closeCallback)
		{
			AvailableGenders = App.DatabaseDependenciesContainer.Resolve<IGendersRepository>().List();
			AvailableGenders.First();

			this.Repository = citizensRepository;

			SaveCommand = new RelayCommand((x) => Save());
			this.CloseCallback = closeCallback;

			NewCitizen = new Citizen("", "", "", "", AvailableGenders.First().Id, DateTime.Now);
		}

		void Save()
		{
			if(SelectedGender == null)
			{
				MessageBox.Show("Пожалуйста, выберите пол");
				return;
			}

			try
			{
				NewCitizen.GenderId = SelectedGender.Id;
				Repository.AddCitizen(NewCitizen);
				CloseCallback?.Invoke();
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		#region NotifyPropertyChanged

		public event PropertyChangedEventHandler PropertyChanged;

		private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion NotifyPropertyChanged
	}
}
