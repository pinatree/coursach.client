﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using CoursachClient.Helpers;
using CoursachClient.View.Windows;
using CoursachClient.ViewModel.Architecture;
using CoursachClient.View.Reports;
using CoursachClient.View.Elements;

using Coursach.DbConnection.Repositories;
using Coursach.DbConnection.Models;

using Autofac;

namespace CoursachClient.ViewModel
{
    public class CitizensScreenViewModel : ItemsDemonstrator<Citizen>
    {
        ICitizensRepository CitizensRepository;

        public RelayCommand OpenCitizenCommand { get; set; }
        public RelayCommand CreateCitizenCommand { get; set; }
        public RelayCommand DeleteCitizenCommand { get; set; }
        
        public RelayCommand UpdatePassportCommand { get; set; }

        public RelayCommand OpenCitizensReportCommand { get; set; }

        public override IEnumerable<Citizen> Items
        {
            get => CitizensRepository.List();
        }

        private Citizen selectedItem;
        public override Citizen SelectedItem
        {
            get => selectedItem;
            set
            {
                if(value != selectedItem)
                {
                    selectedItem = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public CitizensScreenViewModel(ICitizensRepository citizensRepository)
        {
            this.CitizensRepository = citizensRepository;

            OpenCitizenCommand = new RelayCommand((x) => OpenCitizen());
            CreateCitizenCommand = new RelayCommand((x) => CreateCitizen());
            DeleteCitizenCommand = new RelayCommand((x) => DeleteCitizen());
            UpdatePassportCommand = new RelayCommand((x) => UpdatePassport());

            OpenCitizensReportCommand = new RelayCommand((x) =>
            {
                new CitizensReportWindow().ShowDialog();
            });
        }

        void OpenCitizen()
        {
            if(SelectedItem != null)
            {
                var citizenWindow = new CitizenInfo(SelectedItem, CitizensRepository);
                citizenWindow.ShowDialog();
                NotifyPropertyChanged("Items");
            }               
        }

        void CreateCitizen()
        {
            var citizenCreationWindow = new CreateCitizen(SelectedItem, CitizensRepository);
            citizenCreationWindow.ShowDialog();
            NotifyPropertyChanged("Items");
        }

        void DeleteCitizen()
        {
            if (SelectedItem == null)
                return;

            DialogResult dialogResult = System.Windows.Forms.MessageBox.Show("Вы уверены, что хотите удалить пользователя с паспортом " + SelectedItem.Passport + "?",
                "Вы уверены?", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.No)
                return;

            CitizensRepository.DeleteCitizen(SelectedItem.Passport);

            NotifyPropertyChanged("Items");
        }

        void UpdatePassport()
        {
            if (SelectedItem == null)
                return;

            var wind = new InputBox("Новое значение паспорта", "Пожалуйста, введите паспорт");
            wind.ShowDialog();
            if (wind.Success && wind.Result != "")
            {
                try
                {
                    CitizensRepository.UpdatePassport(SelectedItem.Passport, wind.Result);
                    NotifyPropertyChanged("Items");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        public static CitizensScreenViewModel GetAutofacInjected()
        {
			ICitizensRepository citizensRepository = App.DatabaseDependenciesContainer.Resolve<ICitizensRepository>();

            return new CitizensScreenViewModel(citizensRepository);
        }
    }    
}
