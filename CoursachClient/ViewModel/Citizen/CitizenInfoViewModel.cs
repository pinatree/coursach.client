﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using Autofac;
using Coursach.DbConnection.GendersRepositories;
using Coursach.DbConnection.Models;
using Coursach.DbConnection.Repositories;
using CoursachClient.Helpers;

namespace CoursachClient.ViewModel
{
	public class CitizenInfoViewModel : INotifyPropertyChanged
	{
		ICitizensRepository Repository;
		Action CloseCallback;

		public IEnumerable<Gender> AvailableGenders { get; }
		public Gender SelectedGender { get; set; }

		public Citizen Citizen { get; set; }

		public CitizenInfoViewModel(Citizen citizen, ICitizensRepository citizenAccessor, Action closeCallback)
		{
			Citizen newCitizen = new Citizen(citizen.Passport, citizen.Surname, citizen.Name,
				citizen.Patronymic, citizen.GenderId, citizen.BirthDate);

			this.Citizen = newCitizen;


			AvailableGenders = App.DatabaseDependenciesContainer.Resolve<IGendersRepository>().List();
			SelectedGender = AvailableGenders.FirstOrDefault(gender => gender.Id == citizen.GenderId);

			this.Repository = citizenAccessor;

			SaveCommand = new RelayCommand((x) => Save());
			this.CloseCallback = closeCallback;
		}

		public RelayCommand SaveCommand { get; set; }

		void Save()
		{
			try
			{
				Citizen.GenderId = SelectedGender.Id;
				Repository.UpdateCitizen(Citizen);
				CloseCallback?.Invoke();
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		#region NotifyPropertyChanged

		public event PropertyChangedEventHandler PropertyChanged;

		private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion NotifyPropertyChanged
	}
}
