﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;
using System.Runtime.CompilerServices;


namespace CoursachClient.AppConfiguration.Database
{
	public interface IDBConfiguration : INotifyPropertyChanged
	{
		bool UseRelativePath { get; set; }
		string DBLocation { get; set; }
		string DBPassword { get; set; }
	}
}
