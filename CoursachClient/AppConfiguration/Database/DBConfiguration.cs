﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Configuration;

using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace CoursachClient.AppConfiguration.Database
{
	public class DBConfiguration : IDBConfiguration
	{
		public static IDBConfiguration Instance = new DBConfiguration();
		
		public bool UseRelativePath
		{
			get
			{
				return Convert.ToBoolean(ConfigurationManager.AppSettings["useRelativePath"]);
			}
			set
			{
				Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
				configuration.AppSettings.Settings["useRelativePath"].Value = value.ToString();
				configuration.Save();

				ConfigurationManager.RefreshSection("appSettings");

				NotifyPropertyChanged();
			}
		}


		public string DBPassword
		{
			get
			{
				return ConfigurationManager.AppSettings["dbPassword"];
			}
			set
			{
				Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
				configuration.AppSettings.Settings["dbPassword"].Value = value.ToString();
				configuration.Save();

				ConfigurationManager.RefreshSection("appSettings");

				NotifyPropertyChanged();
			}
		}

		public string DBLocation
		{
			get
			{
				return ConfigurationManager.AppSettings["dbLocation"];
			}
			set
			{
				Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
				configuration.AppSettings.Settings["dbLocation"].Value = value.ToString();
				configuration.Save();

				ConfigurationManager.RefreshSection("appSettings");

				NotifyPropertyChanged();
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
