﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Configuration;
using System.Collections.Specialized;

using System.ComponentModel;
using System.Runtime.CompilerServices;

using CoursachClient.Helpers;

namespace CoursachClient.AppConfiguration.Themes
{
	public interface IThemeConfiguration : INotifyPropertyChanged
	{
		ThemeInfo SelectedTheme { get; set; }

		List<ThemeInfo> AvailableThemes { get; }
	}
}
