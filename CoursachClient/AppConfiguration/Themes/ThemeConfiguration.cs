﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace CoursachClient.AppConfiguration.Themes
{
	public class ThemeConfiguration : IThemeConfiguration
	{
		public static IThemeConfiguration Instance { get; set; } = new ThemeConfiguration();

		public ThemeInfo SelectedTheme
		{
			get
			{
				string currentThemeFromConfig = ConfigurationManager.AppSettings["selected_theme"];

				var found = AvailableThemes.FirstOrDefault(theme => theme.Name == currentThemeFromConfig);

				if (found == null)
					return ThemeInfo.ErrorTheme;
				else
					return found;
			}
			set
			{
				if (value == null)
					throw new NullReferenceException();
				
				Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

				string oldThemeName = configuration.AppSettings.Settings["selected_theme"].Value;

				if(oldThemeName != value.Name)
				{
					configuration.AppSettings.Settings["selected_theme"].Value = value.Name;
					configuration.Save();

					ConfigurationManager.RefreshSection("appSettings");

					NotifyPropertyChanged();
				}
			}
		}

		public List<ThemeInfo> AvailableThemes
		{
			get
			{
				List<ThemeInfo> themes = new List<ThemeInfo>();

				var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
				ConfigurationSectionGroup fileCheckersGroup = config.SectionGroups["ColorThemes"];
				foreach (ConfigurationSection section in fileCheckersGroup.Sections)
				{
					NameValueCollection sectionSettings = ConfigurationManager.GetSection(section.SectionInformation.SectionName) as NameValueCollection;

					string newBackground = sectionSettings["background"];
					string newForeground = sectionSettings["foreground"];

					ThemeInfo newTheme = new ThemeInfo()
					{
						Name = section.SectionInformation.Name,
						Background = newBackground,
						Foreground = newForeground
					};

					themes.Add(newTheme);
				}

				return themes;
			}
		}	

		public event PropertyChangedEventHandler PropertyChanged;

		private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
