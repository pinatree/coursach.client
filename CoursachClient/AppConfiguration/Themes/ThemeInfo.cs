﻿namespace CoursachClient.AppConfiguration.Themes
{
	public class ThemeInfo
	{
		public string Name { get; set; }
		public string Background { get; set; }
		public string Foreground { get; set; }

		public static ThemeInfo ErrorTheme = new ThemeInfo()
		{
			Name = "ErrorTheme",
			Background = "gray"
		};
	}
}
