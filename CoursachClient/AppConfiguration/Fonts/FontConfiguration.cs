﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;

using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace CoursachClient.AppConfiguration.Fonts
{
	public class FontConfiguration : IFontConfiguration
	{
		const string SELECTED_FONT_SIZE_CONFIG_KEY = "selected_font_size";
		const int ErrorFontSize = 15;

		public static IFontConfiguration Instance { get; set; } = new FontConfiguration();

		public int FontSize
		{
			get
			{
				try
				{
					return int.Parse(ConfigurationManager.AppSettings["selected_font_size"]);
				}
				catch
				{
					return ErrorFontSize;
				}
			}
			set
			{
				Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
				configuration.AppSettings.Settings[SELECTED_FONT_SIZE_CONFIG_KEY].Value =  value.ToString();
				configuration.Save();

				ConfigurationManager.RefreshSection("appSettings");

				NotifyPropertyChanged();
			}
		}

		public List<int> AvailableFontSizes
		{
			get
			{
				var font_sizes = ConfigurationManager.AppSettings["font_sizes"];

				return font_sizes.Split(',').Select(item => int.Parse(item)).ToList();
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
