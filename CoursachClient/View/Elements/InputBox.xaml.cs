﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;

namespace CoursachClient.View.Elements
{
	public partial class InputBox : Window
	{
		public InputBox(string title, string question)
		{
			InitializeComponent();
			this.Title = title;
			Question.Content = question;
		}
		public bool Success = false;

		public string Result
		{
			get => ResultTB.Text;
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			Success = true;
			Close();
		}
	}
}
