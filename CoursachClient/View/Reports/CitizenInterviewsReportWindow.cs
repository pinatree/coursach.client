﻿using Autofac;
using Coursach.DbConnection.InterviewRepositories;
using Coursach.DbConnection.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoursachClient.View.Reports
{
	public partial class CitizenInterviewsReportWindow : Form
	{
		public CitizenInterviewsReportWindow()
		{
			InitializeComponent();
		}

		private void CitizenInterviewsReportWindow_Load(object sender, EventArgs e)
		{
			this.собеседованиеTableAdapter.Connection = App.DatabaseConnection.myConnection;
			
			// TODO: данная строка кода позволяет загрузить данные в таблицу "cursovaya_DBSet.CitizenInterviews". При необходимости она может быть перемещена или удалена.
			this.citizenInterviewsTableAdapter.Fill(this.cursovaya_DBSet.CitizenInterviews);

			this.reportViewer1.RefreshReport();
		}

		Interview RowToInterview(DataRow row)
		{
			Interview newInterview = new Interview(
				passport: Convert.ToString(row["Паспорт"]),
				comRegNum: Convert.ToString(row["РегНомКомпании"]),
				vacancyId: Convert.ToInt32(row["КодВакансии"]),
				creationDate: Convert.ToDateTime(row["ДатаСоздания"]),
				interviewDate: Convert.ToDateTime(row["ДатаСобеседования"]),
				success: Convert.ToBoolean(row["Трудоустроен"]));

			return newInterview;
		}
	}
}
