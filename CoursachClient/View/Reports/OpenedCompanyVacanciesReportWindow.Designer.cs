﻿namespace CoursachClient.View.Reports
{
	partial class OpenedCompanyVacanciesReportWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
			this.openedCompanyVacanciesBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.cursovaya_DBSet = new CoursachClient.Cursovaya_DBSet();
			this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
			this.openedCompanyVacanciesTableAdapter = new CoursachClient.Cursovaya_DBSetTableAdapters.OpenedCompanyVacanciesTableAdapter();
			((System.ComponentModel.ISupportInitialize)(this.openedCompanyVacanciesBindingSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cursovaya_DBSet)).BeginInit();
			this.SuspendLayout();
			// 
			// openedCompanyVacanciesBindingSource
			// 
			this.openedCompanyVacanciesBindingSource.DataMember = "OpenedCompanyVacancies";
			this.openedCompanyVacanciesBindingSource.DataSource = this.cursovaya_DBSet;
			// 
			// cursovaya_DBSet
			// 
			this.cursovaya_DBSet.DataSetName = "Cursovaya_DBSet";
			this.cursovaya_DBSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
			// 
			// reportViewer1
			// 
			this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
			reportDataSource1.Name = "DataSet1";
			reportDataSource1.Value = this.openedCompanyVacanciesBindingSource;
			this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
			this.reportViewer1.LocalReport.ReportEmbeddedResource = "CoursachClient.View.Reports.OpenedCompanyVacanciesReport.rdlc";
			this.reportViewer1.Location = new System.Drawing.Point(0, 0);
			this.reportViewer1.Name = "reportViewer1";
			this.reportViewer1.ServerReport.BearerToken = null;
			this.reportViewer1.Size = new System.Drawing.Size(697, 374);
			this.reportViewer1.TabIndex = 0;
			// 
			// openedCompanyVacanciesTableAdapter
			// 
			this.openedCompanyVacanciesTableAdapter.ClearBeforeFill = true;
			// 
			// OpenedCompanyVacanciesReportWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(697, 374);
			this.Controls.Add(this.reportViewer1);
			this.Name = "OpenedCompanyVacanciesReportWindow";
			this.Text = "Открытые вакансии компаний";
			this.Load += new System.EventHandler(this.OpenedCompanyVacanciesReportWindow_Load);
			((System.ComponentModel.ISupportInitialize)(this.openedCompanyVacanciesBindingSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cursovaya_DBSet)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
		private Cursovaya_DBSet cursovaya_DBSet;
		private System.Windows.Forms.BindingSource openedCompanyVacanciesBindingSource;
		private Cursovaya_DBSetTableAdapters.OpenedCompanyVacanciesTableAdapter openedCompanyVacanciesTableAdapter;
	}
}