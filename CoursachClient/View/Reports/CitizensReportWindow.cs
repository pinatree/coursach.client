﻿using System;
using System.Windows.Forms;

namespace CoursachClient.View.Reports
{
	public partial class CitizensReportWindow : Form
	{
		public CitizensReportWindow()
		{
			InitializeComponent();
		}

		private void ReportWindow_Load(object sender, EventArgs e)
		{
			//Задаем наше соединение, хранящееся в классе App
			this.гражданинTableAdapter.Connection = App.DatabaseConnection.myConnection;
			
			// TODO: данная строка кода позволяет загрузить данные в таблицу "Cursovaya_DBSet.Гражданин". При необходимости она может быть перемещена или удалена.
			this.гражданинTableAdapter.Fill(this.Cursovaya_DBSet.Гражданин);

			//this.reportViewer1.RefreshReport();
			this.reportViewer1.RefreshReport();
		}
	}
}
