﻿using System;
using System.Windows.Forms;

namespace CoursachClient.View.Reports
{
	public partial class OpenedCompanyVacanciesReportWindow : Form
	{
		public OpenedCompanyVacanciesReportWindow()
		{
			InitializeComponent();
		}

		private void OpenedCompanyVacanciesReportWindow_Load(object sender, EventArgs e)
		{
			this.openedCompanyVacanciesTableAdapter.Connection = App.DatabaseConnection.myConnection;

			// TODO: данная строка кода позволяет загрузить данные в таблицу "cursovaya_DBSet.OpenedCompanyVacancies". При необходимости она может быть перемещена или удалена.
			this.openedCompanyVacanciesTableAdapter.Fill(this.cursovaya_DBSet.OpenedCompanyVacancies);

			this.reportViewer1.RefreshReport();
		}
	}
}
