﻿namespace CoursachClient.View.Reports
{
	partial class CitizensReportWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
			this.гражданинBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.cursovayaDBSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.Cursovaya_DBSet = new CoursachClient.Cursovaya_DBSet();
			this.гражданинTableAdapter = new CoursachClient.Cursovaya_DBSetTableAdapters.ГражданинTableAdapter();
			this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
			((System.ComponentModel.ISupportInitialize)(this.гражданинBindingSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cursovayaDBSetBindingSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Cursovaya_DBSet)).BeginInit();
			this.SuspendLayout();
			// 
			// гражданинBindingSource
			// 
			this.гражданинBindingSource.DataMember = "Гражданин";
			this.гражданинBindingSource.DataSource = this.cursovayaDBSetBindingSource;
			// 
			// cursovayaDBSetBindingSource
			// 
			this.cursovayaDBSetBindingSource.DataSource = this.Cursovaya_DBSet;
			this.cursovayaDBSetBindingSource.Position = 0;
			// 
			// Cursovaya_DBSet
			// 
			this.Cursovaya_DBSet.DataSetName = "Cursovaya_DBSet";
			this.Cursovaya_DBSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
			// 
			// гражданинTableAdapter
			// 
			this.гражданинTableAdapter.ClearBeforeFill = true;
			// 
			// reportViewer1
			// 
			this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
			reportDataSource1.Name = "Cursovaya_DBSet";
			reportDataSource1.Value = this.гражданинBindingSource;
			this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
			this.reportViewer1.LocalReport.ReportEmbeddedResource = "CoursachClient.View.Reports.CitizensReport.rdlc";
			this.reportViewer1.Location = new System.Drawing.Point(0, 0);
			this.reportViewer1.Name = "reportViewer1";
			this.reportViewer1.ServerReport.BearerToken = null;
			this.reportViewer1.Size = new System.Drawing.Size(722, 332);
			this.reportViewer1.TabIndex = 0;
			// 
			// CitizensReportWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(722, 332);
			this.Controls.Add(this.reportViewer1);
			this.Name = "CitizensReportWindow";
			this.Text = "Отчет о стоящих на учете гражданах";
			this.Load += new System.EventHandler(this.ReportWindow_Load);
			((System.ComponentModel.ISupportInitialize)(this.гражданинBindingSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cursovayaDBSetBindingSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Cursovaya_DBSet)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion
		private Cursovaya_DBSet Cursovaya_DBSet;
		private System.Windows.Forms.BindingSource cursovayaDBSetBindingSource;
		private System.Windows.Forms.BindingSource гражданинBindingSource;
		private Cursovaya_DBSetTableAdapters.ГражданинTableAdapter гражданинTableAdapter;
		private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
	}
}