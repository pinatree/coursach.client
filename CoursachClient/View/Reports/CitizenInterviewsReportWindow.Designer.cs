﻿namespace CoursachClient.View.Reports
{
	partial class CitizenInterviewsReportWindow
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
			this.CitizenInterviewsBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.cursovaya_DBSet = new CoursachClient.Cursovaya_DBSet();
			this.собеседованиеBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
			this.собеседованиеTableAdapter = new CoursachClient.Cursovaya_DBSetTableAdapters.СобеседованиеTableAdapter();
			this.citizenInterviewsBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
			this.citizenInterviewsTableAdapter = new CoursachClient.Cursovaya_DBSetTableAdapters.CitizenInterviewsTableAdapter();
			((System.ComponentModel.ISupportInitialize)(this.CitizenInterviewsBindingSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cursovaya_DBSet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.собеседованиеBindingSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.citizenInterviewsBindingSource1)).BeginInit();
			this.SuspendLayout();
			// 
			// CitizenInterviewsBindingSource
			// 
			this.CitizenInterviewsBindingSource.DataMember = "CitizenInterviews";
			this.CitizenInterviewsBindingSource.DataSource = this.cursovaya_DBSet;
			// 
			// cursovaya_DBSet
			// 
			this.cursovaya_DBSet.DataSetName = "Cursovaya_DBSet";
			this.cursovaya_DBSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
			// 
			// собеседованиеBindingSource
			// 
			this.собеседованиеBindingSource.DataMember = "Собеседование";
			this.собеседованиеBindingSource.DataSource = this.cursovaya_DBSet;
			// 
			// reportViewer1
			// 
			this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
			reportDataSource2.Name = "DataSet1";
			reportDataSource2.Value = this.CitizenInterviewsBindingSource;
			this.reportViewer1.LocalReport.DataSources.Add(reportDataSource2);
			this.reportViewer1.LocalReport.ReportEmbeddedResource = "CoursachClient.View.Reports.CitizensInterviewsReports.rdlc";
			this.reportViewer1.Location = new System.Drawing.Point(0, 0);
			this.reportViewer1.Name = "reportViewer1";
			this.reportViewer1.ServerReport.BearerToken = null;
			this.reportViewer1.Size = new System.Drawing.Size(889, 378);
			this.reportViewer1.TabIndex = 0;
			// 
			// собеседованиеTableAdapter
			// 
			this.собеседованиеTableAdapter.ClearBeforeFill = true;
			// 
			// citizenInterviewsBindingSource1
			// 
			this.citizenInterviewsBindingSource1.DataMember = "CitizenInterviews";
			this.citizenInterviewsBindingSource1.DataSource = this.cursovaya_DBSet;
			// 
			// citizenInterviewsTableAdapter
			// 
			this.citizenInterviewsTableAdapter.ClearBeforeFill = true;
			// 
			// CitizenInterviewsReportWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(889, 378);
			this.Controls.Add(this.reportViewer1);
			this.Name = "CitizenInterviewsReportWindow";
			this.Text = "Собеседования граждан";
			this.Load += new System.EventHandler(this.CitizenInterviewsReportWindow_Load);
			((System.ComponentModel.ISupportInitialize)(this.CitizenInterviewsBindingSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cursovaya_DBSet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.собеседованиеBindingSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.citizenInterviewsBindingSource1)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
		private Cursovaya_DBSet cursovaya_DBSet;
		private System.Windows.Forms.BindingSource собеседованиеBindingSource;
		private Cursovaya_DBSetTableAdapters.СобеседованиеTableAdapter собеседованиеTableAdapter;
		private System.Windows.Forms.BindingSource CitizenInterviewsBindingSource;
		private System.Windows.Forms.BindingSource citizenInterviewsBindingSource1;
		private Cursovaya_DBSetTableAdapters.CitizenInterviewsTableAdapter citizenInterviewsTableAdapter;
	}
}