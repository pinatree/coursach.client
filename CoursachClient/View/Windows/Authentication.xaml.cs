﻿using System.Windows;
using CoursachClient.ViewModel.Authentication;

namespace CoursachClient.View.Windows
{
	public partial class Authentication : Window
	{
		public Authentication()
		{
			InitializeComponent();

			this.DataContext = new AuthenticationViewModel(Close, DragMove);
		}
	}
}
