﻿using System.Windows;

using CoursachClient.ViewModel;

namespace CoursachClient.View.Windows
{
	public partial class PropertiesWind : Window
	{
		public PropertiesWind()
		{
			InitializeComponent();
			this.DataContext = new PropertiesViewModel();
		}
	}
}
