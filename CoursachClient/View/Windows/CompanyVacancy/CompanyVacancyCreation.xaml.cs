﻿using Autofac;
using Coursach.DbConnection.CompanyVacancyRepositories;
using Coursach.DbConnection.Repositories;
using CoursachClient.ViewModel;
using System.Windows;

namespace CoursachClient.View.Windows.CompanyVacancy
{
	public partial class CompanyVacancyCreation : Window
	{
		public CompanyVacancyCreation()
		{
			InitializeComponent();

			IVacanciesRepository vacanciesRepository =
				App.DatabaseDependenciesContainer.Resolve<IVacanciesRepository>();

			ICompaniesRepository companiesRepository =
				App.DatabaseDependenciesContainer.Resolve<ICompaniesRepository>();

			ICompanyVacanciesRepository comVacRepository =
				App.DatabaseDependenciesContainer.Resolve<ICompanyVacanciesRepository>();

			this.DataContext =
				new CompanyVacancyCreationViewModel(comVacRepository, companiesRepository, vacanciesRepository, Close);
		}
	}
}
