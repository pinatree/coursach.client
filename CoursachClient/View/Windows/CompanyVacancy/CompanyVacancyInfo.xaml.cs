﻿using Autofac;
using Coursach.DbConnection.CompanyVacancyRepositories;
using Coursach.DbConnection.Repositories;
using CoursachClient.ViewModel;
using System.Windows;

namespace CoursachClient.View.Windows.CompanyVacancy
{
	public partial class CompanyVacancyInfo : Window
	{
		public CompanyVacancyInfo(Coursach.DbConnection.Models.CompanyVacancy companyVacancy)
		{
			InitializeComponent();

			IVacanciesRepository vacanciesRepository =
				App.DatabaseDependenciesContainer.Resolve<IVacanciesRepository>();

			ICompanyVacanciesRepository companyVacanciesRepository =
				App.DatabaseDependenciesContainer.Resolve<ICompanyVacanciesRepository>();

			ICompaniesRepository companiesRepository =
				App.DatabaseDependenciesContainer.Resolve<ICompaniesRepository>();

			this.DataContext =
				new CompanyVacancyInfoViewModel(companyVacancy, companiesRepository, vacanciesRepository, companyVacanciesRepository, Close);
		}
	}
}
