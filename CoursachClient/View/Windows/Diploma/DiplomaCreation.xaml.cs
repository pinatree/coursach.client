﻿using Autofac;
using Coursach.DbConnection.DiplomasRepositories;
using Coursach.DbConnection.EducationLevelsRepositories;
using Coursach.DbConnection.InstitutesRepositories;
using Coursach.DbConnection.Repositories;
using CoursachClient.ViewModel;
using System.Windows;

namespace CoursachClient.View.Windows.Diploma
{
	public partial class DiplomaCreation : Window
	{
		public DiplomaCreation()
		{
			InitializeComponent();
			IInstitutesRepository institutesRepository =
				App.DatabaseDependenciesContainer.Resolve<IInstitutesRepository>();

			IEducationLevelsRepository educationLevelsRepository =
				App.DatabaseDependenciesContainer.Resolve<IEducationLevelsRepository>();

			IDiplomasRepository diplomasRepository =
				App.DatabaseDependenciesContainer.Resolve<IDiplomasRepository>();

			ICitizensRepository citizensRepository =
				App.DatabaseDependenciesContainer.Resolve<ICitizensRepository>();


			this.DataContext = new DiplomaCreationViewModel(citizensRepository, institutesRepository,
				educationLevelsRepository,
				diplomasRepository, Close);
		}
	}
}
