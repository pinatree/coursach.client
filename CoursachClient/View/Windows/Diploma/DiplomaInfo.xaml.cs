﻿using Autofac;
using Coursach.DbConnection.DiplomasRepositories;
using Coursach.DbConnection.EducationLevelsRepositories;
using Coursach.DbConnection.InstitutesRepositories;
using CoursachClient.ViewModel;
using System.Windows;
using Coursach.DbConnection.Repositories;

namespace CoursachClient.View.Windows.Diploma
{
	public partial class DiplomaInfo : Window
	{
		public DiplomaInfo(Coursach.DbConnection.Models.Diploma change)
		{
			InitializeComponent();
			IInstitutesRepository institutesRepository =
				App.DatabaseDependenciesContainer.Resolve<IInstitutesRepository>();

			IEducationLevelsRepository educationLevelsRepository =
				App.DatabaseDependenciesContainer.Resolve<IEducationLevelsRepository>();

			IDiplomasRepository diplomasRepository =
				App.DatabaseDependenciesContainer.Resolve<IDiplomasRepository>();

			ICitizensRepository citizensRepository =
				App.DatabaseDependenciesContainer.Resolve<ICitizensRepository>();

			this.DataContext = new DiplomaInfoViewModel(change, citizensRepository, institutesRepository,
				educationLevelsRepository,
				diplomasRepository, Close);
		}
	}
}
