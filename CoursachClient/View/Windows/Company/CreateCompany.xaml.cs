﻿using Autofac;
using Coursach.DbConnection.Repositories;
using CoursachClient.ViewModel;
using System.Windows;

namespace CoursachClient.View.Windows.Company
{
	public partial class CreateCompany : Window
	{
		public CreateCompany()
		{
			InitializeComponent();

			ICompaniesRepository companiesRepository = App.DatabaseDependenciesContainer.Resolve<ICompaniesRepository>();

			this.DataContext = new CompanyCreationViewModel(companiesRepository, Close);
		}
	}
}
