﻿using Autofac;
using Coursach.DbConnection.CompanyVacancyRepositories;
using Coursach.DbConnection.InterviewRepositories;
using Coursach.DbConnection.Repositories;
using CoursachClient.ViewModel;
using System.Windows;

namespace CoursachClient.View.Windows.Interview
{
	public partial class InterviewCreation : Window
	{
		public InterviewCreation()
		{
			InitializeComponent();

			ICompanyVacanciesRepository companyVacanciesRepository =
				App.DatabaseDependenciesContainer.Resolve<ICompanyVacanciesRepository>();

			ICitizensRepository citizensRepository =
				App.DatabaseDependenciesContainer.Resolve<ICitizensRepository>();

			IInterviewsRepository interviewsRepository =
				App.DatabaseDependenciesContainer.Resolve<IInterviewsRepository>();

			this.DataContext = new InterviewCreationViewModel(companyVacanciesRepository, citizensRepository, interviewsRepository, Close);
		}
	}
}
