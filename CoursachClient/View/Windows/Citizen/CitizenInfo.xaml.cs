﻿using Coursach.DbConnection.Models;
using Coursach.DbConnection.Repositories;
using CoursachClient.ViewModel;
using System.Windows;

namespace CoursachClient.View.Windows
{
	public partial class CitizenInfo : Window
	{
		public CitizenInfo(Citizen citizen, ICitizensRepository citizenAccessor)
		{
			InitializeComponent();
			this.DataContext = new CitizenInfoViewModel(citizen, citizenAccessor, Close);
		}
	}
}
