﻿using Coursach.DbConnection.Models;
using Coursach.DbConnection.Repositories;
using CoursachClient.ViewModel;
using System.Windows;

namespace CoursachClient.View.Windows
{
	public partial class CreateCitizen : Window
	{
		public CitizenCreationViewModel CitizenCreationViewModel;

		public CreateCitizen(Citizen citizen, ICitizensRepository citizenAccessor)
		{
			InitializeComponent();
			CitizenCreationViewModel = new CitizenCreationViewModel(citizenAccessor, Close);
			this.DataContext = CitizenCreationViewModel;
		}
	}
}
