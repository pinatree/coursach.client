﻿using System.Windows.Controls;
using Autofac;
using Coursach.DbConnection.CompanyVacancyRepositories;
using CoursachClient.ViewModel;

namespace CoursachClient.View.Pages
{
	public partial class CompanyVacanciesScreen : UserControl
	{
		public CompanyVacanciesScreen()
		{
			InitializeComponent();
			var companyVacanciesRepository = App.DatabaseDependenciesContainer.Resolve<ICompanyVacanciesRepository>();
			this.DataContext = new CompanyVacanciesViewModel(companyVacanciesRepository);
		}
	}
}
