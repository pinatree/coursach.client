﻿using System.Windows.Controls;
using Autofac;
using Coursach.DbConnection.Repositories;
using CoursachClient.ViewModel;

namespace CoursachClient.View.Pages
{
	public partial class VacanciesScreen : UserControl
	{
		public VacanciesScreen()
		{
			InitializeComponent();
			var vacanciesRepository = App.DatabaseDependenciesContainer.Resolve<IVacanciesRepository>();
			this.DataContext = new VacanciesViewModel(vacanciesRepository);
		}
	}
}
