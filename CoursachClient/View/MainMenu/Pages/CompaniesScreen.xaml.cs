﻿using System.Windows.Controls;

using CoursachClient.ViewModel;

namespace CoursachClient.View.Pages
{
	public partial class CompaniesScreen : UserControl
	{
		public CompaniesScreen()
		{
			InitializeComponent();
			this.DataContext = CompaniesScreenViewModel.GetAutofacInjected();
		}
	}
}
