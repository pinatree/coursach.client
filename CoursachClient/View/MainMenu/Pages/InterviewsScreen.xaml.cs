﻿using System.Windows.Controls;
using Autofac;
using Coursach.DbConnection.InterviewRepositories;
using CoursachClient.ViewModel;

namespace CoursachClient.View.Pages
{
	public partial class InterviewsScreen : UserControl
	{
		public InterviewsScreen()
		{
			InitializeComponent();
			var interviewsRepository = App.DatabaseDependenciesContainer.Resolve<IInterviewsRepository>();
			this.DataContext = new InterviewViewModel(interviewsRepository);
		}
	}
}
