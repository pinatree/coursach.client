﻿using System.Windows.Controls;

using CoursachClient.ViewModel;

namespace CoursachClient.View.Pages
{
	public partial class CitizensScreen : UserControl
	{
		public CitizensScreen()
		{
			InitializeComponent();
			this.DataContext = CitizensScreenViewModel.GetAutofacInjected();
		}
	}
}
