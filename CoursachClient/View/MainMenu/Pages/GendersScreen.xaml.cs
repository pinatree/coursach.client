﻿using System.Windows.Controls;
using Autofac;
using Coursach.DbConnection.GendersRepositories;
using CoursachClient.ViewModel;

namespace CoursachClient.View.Pages
{
	public partial class GendersScreen : UserControl
	{
		public GendersScreen()
		{
			InitializeComponent();
			var gendersRepository = App.DatabaseDependenciesContainer.Resolve<IGendersRepository>();
			this.DataContext = new GendersViewModel(gendersRepository);
		}
	}
}
