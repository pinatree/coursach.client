﻿using System.Windows.Controls;
using Autofac;
using Coursach.DbConnection.DiplomasRepositories;
using CoursachClient.ViewModel;

namespace CoursachClient.View.Pages
{
	public partial class DiplomasScreen : UserControl
	{
		public DiplomasScreen()
		{
			InitializeComponent();
			var diplomasRepository = App.DatabaseDependenciesContainer.Resolve<IDiplomasRepository>();
			this.DataContext = new DiplomasViewModel(diplomasRepository);
		}
	}
}
