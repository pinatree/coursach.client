﻿using System.Windows.Controls;
using Autofac;
using Coursach.DbConnection.InstitutesRepositories;
using CoursachClient.ViewModel;

namespace CoursachClient.View.Pages
{
	public partial class InstitutesScreen : UserControl
	{
		public InstitutesScreen()
		{
			InitializeComponent();
			var institutesRepository = App.DatabaseDependenciesContainer.Resolve<IInstitutesRepository>();
			this.DataContext = new InstitutesViewModel(institutesRepository);
		}
	}
}
