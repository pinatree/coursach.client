﻿using System.Windows.Controls;
using Autofac;
using Coursach.DbConnection.EducationLevelsRepositories;
using CoursachClient.ViewModel;

namespace CoursachClient.View.Pages
{
	public partial class EducationLevelsScreen : UserControl
	{
		public EducationLevelsScreen()
		{
			InitializeComponent();
			IEducationLevelsRepository repository =
				App.DatabaseDependenciesContainer.Resolve<IEducationLevelsRepository>();
			this.DataContext = new EducationLevelsViewModel(repository);
		}
	}
}
