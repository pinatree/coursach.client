﻿using System;
using System.Windows;

using CoursachClient.ViewModel;

namespace CoursachClient
{
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
			Action closeCallback = Close;
			Action dragMoveCallback = DragMove;

			this.DataContext = new MainWindowViewModel(closeCallback, dragMoveCallback);
		}
	}
}
